section basics
parameters {Sigma : Type}

def word : Type := list Sigma
def lang : Type := word → Prop

end basics


section dfa


structure dfa {Sigma : Type} : Type 1 :=
  (Q : Type)
  (init : Q)
  (final : Q → Prop)
  (δ : Q → Sigma → Q)


def δ_star (A : dfa Sigma) : A.Q → word → A.Q
| q [] := q
| q (x :: w) := δ_star (A.δ q x) w
end dfa

section nfa

structure nfa(Sigma : Type) : Type 1 :=
(Q : Type)
(inits : Q → Prop)
(final : Q → Prop)
(δ : Q → Sigma → Q → Prop)

end nfa




end dfa
