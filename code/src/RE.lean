import tactic
import tactic.rcases
import data.list.join
import data.set.lattice

set_option trace.eqn_compiler.elim_match true

------- universe a means input_char
------- universe b means state
universes a b

section finite_automata
structure dfa (α : Type a) ( q : Type b) :=
(δ : q → α → q)
(init : q)
(final: q → Prop)

structure nfa (α : Type a) ( q : Type b) :=
(inits : q → Prop)
(final : q → Prop)
(δ : q → α → q → Prop)

-- α → Sigma,  q → state
variables {α : Type a} {q : Type b} {dfa_t : dfa α q} {nfa_t : nfa α q}

 -- word 
def word (α)  := list α
-- language
def lang (α)  := list(α) → Prop
-- def lang (α)  := set (list α)

def dfa_δ_star  (A : dfa α q) : q → word α  → q
| q [] := q
| q (x :: w) := dfa_δ_star (A.δ q x) w

def dfa_lang  : dfa α q → lang α
| A w := A.final (dfa_δ_star A A.init w)


def nfa_δ_star (B : nfa α q) : q → word α → q → Prop
| q1 [] q2 := q1 = q2
| q1 (a::l) q2 := ∃ q3: q , B.δ q1 a q3 ∧ nfa_δ_star q3 l q2
variable { B : nfa α q}

-- instance [fintype q] [decidable_eq q] [Π q1 a q3, decidable (B.δ q1 a q3)] :
--   Π q1 a q2, decidable (nfa_δ_star B q1 a q2)
-- | q1 [] q2 := by apply_instance
-- | q1 (a :: l) q2 := fintype.decidable_exists_fintype

def nfa_lang : nfa α q → lang α
| nfa word := ∃q1 q2: q, nfa.inits q1 ∧ nfa.final q2 ∧ nfa_δ_star nfa q1 word q2
-------

-- dfa2nfa
def d2n_init(A : dfa α q): q → Prop
| q := A.init = q 

def d2n_transition (A: dfa α q) : q → α → q → Prop
|q1 a q2 := (A.δ q1 a) = q2

def dfa2nfa : dfa α q → nfa α q
| dfa := { 
           inits := d2n_init dfa,
           final := dfa.final,
           δ := d2n_transition dfa
          } 

-------
-- proof : dfa to nfa

lemma dfa_to_nfa_helper1 (A: dfa α q) (w : word α) : ∀ start : q, nfa_δ_star (dfa2nfa A) start w (dfa_δ_star A start w):=
begin
  induction w with c w' ih,
  {tauto},
  dsimp[nfa_δ_star, dfa_δ_star],
  assume s,
  existsi (A.δ s c),
  constructor,
  dsimp[dfa2nfa,d2n_transition],
  refl,
  apply ih,
end

lemma helper_2nd (A: dfa α q) (w : word α): ∀ start: q, (∃ q2 : q, (dfa2nfa A).final q2 ∧ nfa_δ_star (dfa2nfa A) start w q2) → A.final (dfa_δ_star A start w):=
begin
  induction w with  c w' ih,
  assume start,
  dsimp[dfa2nfa, nfa_δ_star, dfa_δ_star],
  assume m,
  cases m with q2 m,
  cases m with m1 m2,
  rw m2,
  exact m1,
  assume start,
  dsimp[nfa_δ_star, dfa_δ_star],
  assume m,
  apply ih,
  cases m with q2 m,
  cases m with m1 m2,
  cases m2 with q3 m2,
  cases m2 with m2 m3,
  existsi q2,
  constructor,
  exact m1,
  dsimp[dfa2nfa, d2n_transition] at m2,
  rw m2,
  exact m3,
end


lemma dfa_to_nfa_helper2 (A: dfa α q) (w : word α): nfa_lang (dfa2nfa A) w → dfa_lang A w:=
begin
  assume lang,
  dsimp[nfa_lang, dfa_lang] at *,  
  cases lang with q1 h,
  cases h with q2 h,
  cases h with h1 h2,
  cases h2 with h2 h3,
  dsimp [dfa2nfa, d2n_init] at h1,
  dsimp [dfa2nfa, d2n_transition] at h2,
  apply helper_2nd,
  dsimp[dfa2nfa],
  existsi q2,
  constructor,
  exact h2,
  rw h1,
  exact h3,
end


theorem dfa_to_nfa_1: ∀ dfa: dfa α q,  (∀ w : word α,  dfa_lang dfa w ↔ nfa_lang (dfa2nfa dfa) w):=
begin
  assume A,
  assume w,
  constructor,
  assume lang,
  dsimp [ nfa_lang, dfa_lang] at *,
  existsi A.init,
  existsi (dfa_δ_star A A.init w),
  constructor,
  dsimp[dfa2nfa, d2n_init],
  refl,
  constructor,
  exact lang,
  induction w with c w' ih gerializing A.init,
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  refl,
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  existsi (A.δ A.init c),
  constructor,
  dsimp [dfa2nfa, d2n_transition],
  refl,
  apply dfa_to_nfa_helper1,
  --------------
  apply dfa_to_nfa_helper2,
end

theorem dfa_to_nfa: ∀ dfa: dfa α q, ∀ w : word α, 
 dfa_lang dfa w = nfa_lang (dfa2nfa dfa) w:=
begin
  assume A w,
  ext x,
  apply dfa_to_nfa_1,
end

-------


-- nfa2dfa

def n2d_final (B : nfa α q) : (q → Prop) → Prop
| sub_q := ∃ q : q,  sub_q q ∧ B.final q 

-- def nfa_δ_star (B : nfa α) : q → word α→ q → Prop
-- | q1 [] q2 := q1 = q2
-- | q1 (a::l) q2 := ∃ q3: q, B.δ q1 a q3 ∧ nfa_δ_star q3 l q2

def n2d_transition (B : nfa α q) : (q → Prop) → α → q → Prop
| sub_q c q2 := ∃ q1 : q, sub_q q1 ∧ B.δ q1 c q2 

-- def n2d_transition_2 (B : nfa α) : (q → Prop) → α → q → Prop
-- | sub_q c := {q | ∃q1 : q, sub_q q1 ∧ B.δ q1 c q }

def nfa2dfa : nfa α q → dfa α ( q → Prop )
| B := {
        init := B.inits, -- 
        final := n2d_final B, -- state including finals in nfa.
        δ := n2d_transition B, -- set (nfq) → s → set ( nfq )  
        }

lemma n2d_helper_1(B: nfa α q) (w : word α) :∀ start: q → Prop,  (nfa2dfa B).final (dfa_δ_star (nfa2dfa B) start w) →  (∃ (q1 q2 : q), start q1 ∧ B.final q2 ∧ nfa_δ_star B q1 w q2):=
begin
  induction w with c w' ih,
  assume start,
  dsimp[dfa_δ_star, nfa_δ_star, nfa2dfa, n2d_final],
  assume m,
  cases m with q m,
  cases m with m1 m2,
  existsi q, existsi q,
  {tauto},
  assume start,
  -- (∃ (q1 q2 : q), start q1 ∧ B.final q2 ∧ nfa_δ_star B q1 (c :: w') q2)
  -- (∃ (q1 q2 : q), (n2d_transition B start c) q1 ∧ B.final q2 ∧ nfa_δ_star B q1 w' q2
  dsimp[dfa_δ_star, nfa_δ_star, nfa2dfa, n2d_final, n2d_transition] at *,
  assume m,
  have h : ∃ (q1 q2 : q), (n2d_transition B start c) q1 ∧ B.final q2 ∧ nfa_δ_star B q1 w' q2,
  apply ih,
  exact m,
  dsimp[n2d_transition] at h,
  cases h with q1 h, cases h with q2 h,
  cases h with h1 h2,
  cases h1 with q3 h3,
  cases h3 with h3 h4,
  cases h2 with h2 h5,
  existsi q3,
  existsi q2,
  -- constructor, exact h3, constructor, exact h2,
  -- existsi q1,
  -- constructor, exact h4, exact h5,
  {tauto},
end

lemma n2d_helper_2(B: nfa α q) (w : word α) : ∀(q2 : q),  ∀ start: q → Prop, (∃(q1 : q),  start q1 ∧ nfa_δ_star B q1 w q2 )→ dfa_δ_star (nfa2dfa B) start w q2:=
begin
  assume q2,
  induction w with c w' ih,
  assume start,
  assume m,
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  cases m with q1 m,
  cases m with st_q1 eq,
  rw← eq,
  exact st_q1,
  ----------
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  assume start,  
  assume m,
  cases m with q1 m,
  cases m with st_q1 m,
  cases m with q3 m,
  cases m with q1_c_q3 nfa_q3_w'_q2,
  apply ih,
  existsi q3,
  dsimp[nfa2dfa, n2d_transition],
  {tauto},
  -- constructor,
  -- existsi q1,
  -- constructor,
  -- exact st_q1,
  -- exact q1_c_q3,
  -- exact nfa_q3_w'_q2,
end

theorem nfa_to_dfa_1: ∀ nfa: nfa α q,  (∀ w : word α,  dfa_lang (nfa2dfa nfa) w ↔ nfa_lang nfa w):=
begin
  assume B,
  assume w,
  constructor,
  assume lang,
  induction w with c w' ih,
  dsimp [ nfa_lang, dfa_lang] at *,
  dsimp [nfa2dfa, n2d_final, dfa_δ_star, nfa_δ_star] at *,
  cases lang with q h,
  cases h with h1 h2,
  existsi q, existsi q,
  {tauto},
  --------
  dsimp [ nfa_lang, dfa_lang] at *,
  -- dsimp [nfa_δ_star, dfa_δ_star],
  apply n2d_helper_1,
  exact lang,

  assume lang,
  dsimp [ nfa_lang, dfa_lang] at *,
  dsimp [nfa2dfa, n2d_final],
  cases lang with q1 h,
  cases h with q2 h,
  cases h with h1 h2,
  cases h2 with h2 h3,
  existsi q2,
  constructor,
  induction w with c w' ih,
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  rw← h3, exact h1,
  dsimp[nfa_δ_star] at h3,
  dsimp[dfa_δ_star],
  cases h3 with q3 h,
  cases h with h3 h4,
  apply n2d_helper_2,
  dsimp[n2d_transition],
  existsi q3,
  {tauto},
  {tauto},
  -- constructor,
  -- existsi q1,
  -- constructor,
  -- exact h1, exact h3, exact h4,
  -- exact h2,
end

theorem nfa_to_dfa: ∀ nfa : nfa α q, ∀ w : word α, 
 dfa_lang (nfa2dfa nfa) w = nfa_lang nfa w:=
begin
  assume A w,
  ext x,
  apply nfa_to_dfa_1,
end

--- end nfa
end finite_automata



---RE
section regular_expression
variables {α : Type a} [dec : decidable_eq α]
-- 1. ∅ is a regular expression. (empty set)
-- 2. ε is a regular expression. (empty string)
-- 3. For each x ∈ α , x is a regular expression. (literal character)
-- 4. If E and F are regular expressions then E + F is a regular expression. 
-- 5. If E and F are regular expressions then EF (juxtapositioning; just one
-- after the other) is a regular expression. (concatenation)
-- 6. If E is a regular expression then E∗ is a regular expression. (Kleene star) 

-- α is Sigma
-- syntax of regular expression
inductive regular_expression (α : Type a)  : Type a
| zero : regular_expression
| epsilon : regular_expression
| char : α → regular_expression
| plus : regular_expression → regular_expression → regular_expression
| comp : regular_expression → regular_expression → regular_expression
| star : regular_expression → regular_expression

-- def matches : regular_expression α → language α
-- | 0 := 0
-- | 1 := 1
-- | (char a) := {[a]}
-- | (P + Q) := P.matches + Q.matches
-- | (P * Q) := P.matches * Q.matches
-- | (star P) := P.matches.star

open regular_expression
------ instance of regular expression
instance : has_add (regular_expression α) := ⟨plus⟩
instance : has_mul (regular_expression α) := ⟨comp⟩
instance : has_one (regular_expression α) := ⟨epsilon⟩
instance : has_zero (regular_expression α) := ⟨zero⟩
attribute [pattern] has_mul.mul

-- @[simp] lemma zero_def : (zero : regular_expression α) = 0 := rfl
-- @[simp] lemma one_def : (epsilon : regular_expression α) = 1 := rfl

-- @[simp] lemma plus_def (P Q : regular_expression α) : plus P Q = P + Q := rfl
-- @[simp] lemma comp_def (P Q : regular_expression α) : comp P Q = P * Q := rfl
------


------ instance of language

local attribute [reducible] lang

/-- The image of a binary function `f : α → β → γ` as a function `set α → set β → set γ`.
  Mathematically this should be thought of as the image of the corresponding function `α × β → γ`.
-/
-- def image2 (f : α → β → γ) (s : set α) (t : set β) : set γ :=
-- {c | ∃ a b, a ∈ s ∧ b ∈ t ∧ f a b = c }

instance : has_mul (lang α) := ⟨set.image2 (++)⟩
/-- The sum of two languages is the union of  -/
instance : has_add (lang α) := ⟨set.union⟩
/-- Zero language has no elements. -/
instance : has_zero (lang α) := ⟨(∅ : set _)⟩

instance : has_union (lang α) := ⟨set.union⟩

def empty_check: lang α 
| w := false

def epsilon_check : lang α
| [] := true
| (a::l) := false

def char_check: α → lang α
| a w := w = [a]
 
def star_lang: lang α → lang α
| l w := ∃ S : list (list α), w = S.join ∧ ∀ y ∈ S , l y 
 
------ all strings can be accepted by a regular expression
def reg_lang : regular_expression α → lang α
|zero := empty_check
|epsilon := epsilon_check
|(char a) := (char_check a)
|(plus P Q) := (reg_lang P) + (reg_lang Q) -- reg_lang P ∪ reg_lang Q
|(comp P Q) := reg_lang P * reg_lang Q -- reg_lang P ∩ reg_lang Q
|(star P) := star_lang (reg_lang P)

include dec


-- epsilon is successful match
-- zero means terrible match
/-- `match_epsilon P` is true if and only if `P` matches the empty string -/
def match_epsilon : regular_expression α → bool
| zero := ff
| epsilon := tt
| (char _) := ff
| (P + Q) := match_epsilon P ∨ match_epsilon Q
| (P * Q) := match_epsilon P ∧ match_epsilon Q
| (star P) := tt

/-- matches `x` if `P` matches `a :: x`, the Brzozowski derivative of `P` with respect
  to `a` -/ 
def step [decidable_eq α]: regular_expression α → α → regular_expression α
| zero _ := zero
| epsilon _ := zero
| (char a₁) a₂ := if a₁ = a₂ then epsilon else zero -- if accept then should be epsilon
| (P + Q) a := step P a + step Q a  -- or
| (P * Q) a := 
  if match_epsilon P then -- if P can accept empty string (epsilon * star) * reg
    step P a * Q + step Q a -- step Q a
  else
    step P a * Q
| (star P) a := step P a * star P


-- last of the list must be []
/-- `P.rmatch x` is true if and only if `P` matches `x`. This is a computable definition equivalent
  to `matches`. -/
def reg_match : regular_expression α → word α → bool
| P [] := match_epsilon P
| P (a::as) := reg_match (step P a) as


-- semantics

lemma zero_reg_match (x : word α) : reg_match zero x = ff :=
begin
  induction x,
  dsimp[reg_match, match_epsilon],
  refl,
  dsimp[reg_match, match_epsilon, step],
  exact x_ih,
end

lemma epsilon_reg_match (x : word α) : reg_match epsilon x ↔ x = [] :=
-- by induction x; simp [rmatch, match_epsilon, deriv, *]
begin
  induction x with c l ih,
  dsimp[reg_match, match_epsilon, step, *],
  {tauto},
  dsimp[reg_match, match_epsilon, step, *],
  cases ih with ih_1 ih_2,
  constructor,
  assume h,
  rw zero_reg_match at h,
  cases h,
  assume h,
  cases h,
end

lemma char_reg_match (a : α) (x : list α) : reg_match (char a) x ↔ x = [a] :=
begin
  cases x with _ x,
  {tauto},
  cases x,
  rw [reg_match, step],
  split_ifs; -- split if then else
  {tauto},
  rw [reg_match, step],
  split_ifs,
  rw epsilon_reg_match,
  {tauto},
  rw zero_reg_match,
  tauto,
end
--- other semantics


--- TODO : translating between finite automata and regular expression

end regular_expression
