import tactic
set_option trace.eqn_compiler.elim_match true


-- basic element
-- word 
def word (Sigma : Type):= list Sigma

-- language
def lang (Sigma : Type) := word Sigma -> Prop -- set (Word Sigma)
-------------

--- dfa
structure dfa(Sigma : Type) : Type 1 :=
(Q : Type)
(init : Q)
(final : Q → Prop)
(δ : Q → Sigma → Q)

variable {Sigma : Type}

def dfa_δ_star (A : dfa Sigma) : A.Q → word Sigma→ A.Q
| q [] := q
| q (x :: w) := dfa_δ_star (A.δ q x) w

def dfa_lang : dfa Sigma → lang Sigma
| dfa word := dfa.final (dfa_δ_star dfa dfa.init word)

------

-- nfa
structure nfa(Sigma : Type) : Type 1 :=
(Q : Type)
(inits : Q → Prop)
(final : Q → Prop)
(δ : Q → Sigma → Q → Prop)


def nfa_δ_star  (B : nfa Sigma): B.Q → word Sigma→ B.Q → Prop
| q1 [] q2 := q1 = q2
| q1 (a::l) q2 := ∃ q3: B.Q, B.δ q1 a q3 ∧ nfa_δ_star q3 l q2

def nfa_lang: nfa Sigma → lang Sigma
| nfa word := ∃q1 q2: nfa.Q, nfa.inits q1 ∧ nfa.final q2 ∧ nfa_δ_star nfa q1 word q2
-------

-- dfa2nfa
def d2n_init(A : dfa Sigma): A.Q → Prop
| q := A.init = q 

def d2n_transition (A: dfa Sigma) : A.Q → Sigma → A.Q → Prop
|q1 a q2 := (A.δ q1 a) = q2

def dfa2nfa : dfa Sigma → nfa Sigma 
| dfa := { Q := dfa.Q,
           inits := d2n_init dfa,
           final := dfa.final,
           δ := d2n_transition dfa
          } 

-------
-- proof : dfa to nfa

lemma dfa_to_nfa_helper1 (A: dfa Sigma) (w : word Sigma): ∀ start : A.Q, nfa_δ_star (dfa2nfa A) start w (dfa_δ_star A start w):=
begin
  induction w with c w' ih,
  {tauto},
  dsimp[nfa_δ_star, dfa_δ_star],
  assume s,
  existsi (A.δ s c),
  constructor,
  dsimp[dfa2nfa,d2n_transition],
  refl,
  apply ih,
end

lemma helper_2nd (A: dfa Sigma) (w : word Sigma): ∀ start: A.Q, (∃ q2 : (dfa2nfa A).Q, (dfa2nfa A).final q2 ∧ nfa_δ_star (dfa2nfa A) start w q2) → A.final (dfa_δ_star A start w):=
begin
  induction w with  c w' ih,
  assume start,
  dsimp[dfa2nfa, nfa_δ_star, dfa_δ_star],
  assume m,
  cases m with q2 m,
  cases m with m1 m2,
  rw m2,
  exact m1,
  assume start,
  dsimp[nfa_δ_star, dfa_δ_star],
  assume m,
  apply ih,
  cases m with q2 m,
  cases m with m1 m2,
  cases m2 with q3 m2,
  cases m2 with m2 m3,
  existsi q2,
  constructor,
  exact m1,
  dsimp[dfa2nfa, d2n_transition] at m2,
  rw m2,
  exact m3,
end


lemma dfa_to_nfa_helper2 (A: dfa Sigma) (w : word Sigma): nfa_lang (dfa2nfa A) w → dfa_lang A w:=
begin
  assume lang,
  dsimp[nfa_lang, dfa_lang] at *,  
  cases lang with q1 h,
  cases h with q2 h,
  cases h with h1 h2,
  cases h2 with h2 h3,
  dsimp [dfa2nfa, d2n_init] at h1,
  dsimp [dfa2nfa, d2n_transition] at h2,
  apply helper_2nd,
  dsimp[dfa2nfa],
  existsi q2,
  constructor,
  exact h2,
  rw h1,
  exact h3,
end


theorem dfa_to_nfa_1: ∀ dfa: dfa Sigma,  (∀ w : word Sigma,  dfa_lang dfa w ↔ nfa_lang (dfa2nfa dfa) w):=
begin
  assume A,
  assume w,
  constructor,
  assume lang,
  dsimp [ nfa_lang, dfa_lang] at *,
  existsi A.init,
  existsi (dfa_δ_star A A.init w),
  constructor,
  dsimp[dfa2nfa, d2n_init],
  refl,
  constructor,
  exact lang,
  induction w with c w' ih gerializing A.init,
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  refl,
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  existsi (A.δ A.init c),
  constructor,
  dsimp [dfa2nfa, d2n_transition],
  refl,
  apply dfa_to_nfa_helper1,
  --------------
  apply dfa_to_nfa_helper2,
end

theorem dfa_to_nfa: ∀ dfa: dfa Sigma, ∀ w : word Sigma, 
 dfa_lang dfa w = nfa_lang (dfa2nfa dfa) w:=
begin
  assume A w,
  ext x,
  apply dfa_to_nfa_1,
end

-------


-- nfa2dfa

def n2d_final (B : nfa Sigma) : (B.Q → Prop) → Prop
| sub_q := ∃ q : B.Q,  sub_q q ∧ B.final q 

-- def nfa_δ_star (B : nfa Sigma) : B.Q → word Sigma→ B.Q → Prop
-- | q1 [] q2 := q1 = q2
-- | q1 (a::l) q2 := ∃ q3: B.Q, B.δ q1 a q3 ∧ nfa_δ_star q3 l q2

def n2d_transition (B : nfa Sigma) : (B.Q → Prop) → Sigma → B.Q → Prop
| sub_q c q2 := ∃ q1 : B.Q, sub_q q1 ∧ B.δ q1 c q2 

-- def n2d_transition_2 (B : nfa Sigma) : (B.Q → Prop) → Sigma → B.Q → Prop
-- | sub_q c := {q | ∃q1 : B.Q, sub_q q1 ∧ B.δ q1 c q }

def nfa2dfa : nfa Sigma → dfa Sigma
| B := {
        Q := B.Q → Prop, -- set (nfa.Q)
        init := B.inits, -- 
        final := n2d_final B, -- state including finals in nfa.
        δ := n2d_transition B, -- set (nfa.Q) → s → set ( nfa.Q )  
        }

lemma n2d_helper_1(B: nfa Sigma) (w : word Sigma) :∀ start: (nfa2dfa B).Q,  (nfa2dfa B).final (dfa_δ_star (nfa2dfa B) start w) →  (∃ (q1 q2 : B.Q), start q1 ∧ B.final q2 ∧ nfa_δ_star B q1 w q2):=
begin
  induction w with c w' ih,
  assume start,
  dsimp[dfa_δ_star, nfa_δ_star, nfa2dfa, n2d_final],
  assume m,
  cases m with q m,
  cases m with m1 m2,
  existsi q, existsi q,
  {tauto},
  assume start,
  -- (∃ (q1 q2 : B.Q), start q1 ∧ B.final q2 ∧ nfa_δ_star B q1 (c :: w') q2)
  -- (∃ (q1 q2 : B.Q), (n2d_transition B start c) q1 ∧ B.final q2 ∧ nfa_δ_star B q1 w' q2
  dsimp[dfa_δ_star, nfa_δ_star, nfa2dfa, n2d_final, n2d_transition] at *,
  assume m,
  have h : ∃ (q1 q2 : B.Q), (n2d_transition B start c) q1 ∧ B.final q2 ∧ nfa_δ_star B q1 w' q2,
  apply ih,
  exact m,
  dsimp[n2d_transition] at h,
  cases h with q1 h, cases h with q2 h,
  cases h with h1 h2,
  cases h1 with q3 h3,
  cases h3 with h3 h4,
  cases h2 with h2 h5,
  existsi q3,
  existsi q2,
  -- constructor, exact h3, constructor, exact h2,
  -- existsi q1,
  -- constructor, exact h4, exact h5,
  {tauto},
end

lemma n2d_helper_2(B: nfa Sigma) (w : word Sigma) : ∀(q2 : B.Q),  ∀ start: (nfa2dfa B).Q, (∃(q1 : B.Q),  start q1 ∧ nfa_δ_star B q1 w q2 )→ dfa_δ_star (nfa2dfa B) start w q2:=
begin
  assume q2,
  induction w with c w' ih,
  assume start,
  assume m,
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  cases m with q1 m,
  cases m with st_q1 eq,
  rw← eq,
  exact st_q1,
  ----------
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  assume start,  
  assume m,
  cases m with q1 m,
  cases m with st_q1 m,
  cases m with q3 m,
  cases m with q1_c_q3 nfa_q3_w'_q2,
  apply ih,
  existsi q3,
  dsimp[nfa2dfa, n2d_transition],
  {tauto},
  -- constructor,
  -- existsi q1,
  -- constructor,
  -- exact st_q1,
  -- exact q1_c_q3,
  -- exact nfa_q3_w'_q2,
end

theorem nfa_to_dfa_1: ∀ nfa: nfa Sigma,  (∀ w : word Sigma,  dfa_lang (nfa2dfa nfa) w ↔ nfa_lang nfa w):=
begin
  assume B,
  assume w,
  constructor,
  assume lang,
  induction w with c w' ih,
  dsimp [ nfa_lang, dfa_lang] at *,
  dsimp [nfa2dfa, n2d_final, dfa_δ_star, nfa_δ_star] at *,
  cases lang with q h,
  cases h with h1 h2,
  existsi q, existsi q,
  {tauto},
  --------
  dsimp [ nfa_lang, dfa_lang] at *,
  -- dsimp [nfa_δ_star, dfa_δ_star],
  apply n2d_helper_1,
  exact lang,

  assume lang,
  dsimp [ nfa_lang, dfa_lang] at *,
  dsimp [nfa2dfa, n2d_final],
  cases lang with q1 h,
  cases h with q2 h,
  cases h with h1 h2,
  cases h2 with h2 h3,
  existsi q2,
  constructor,
  induction w with c w' ih,
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  rw← h3, exact h1,
  dsimp[nfa_δ_star] at h3,
  dsimp[dfa_δ_star],
  cases h3 with q3 h,
  cases h with h3 h4,
  apply n2d_helper_2,
  dsimp[n2d_transition],
  existsi q3,
  {tauto},
  {tauto},
  -- constructor,
  -- existsi q1,
  -- constructor,
  -- exact h1, exact h3, exact h4,
  -- exact h2,
end


theorem nfa_to_dfa: ∀ nfa : nfa Sigma, ∀ w : word Sigma, 
 dfa_lang (nfa2dfa nfa) w = nfa_lang nfa w:=
begin
  assume A w,
  ext x,
  apply nfa_to_dfa_1,
end


section test
variable {A : dfa Sigma}
variable {q : A.Q}

#check decidable (A.final q)

end test