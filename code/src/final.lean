/-
Copyright (c) 2022 Yunlong LIN. 
Author: Yunlong LIN
Introduction : This file describe the first step of formalize automata theory
including DFA, NFA, from DFA to NFA, from NFA to DFA, some parts of regular expression

TODO: 
* [decidable]
* fromalize the equivalence between regular expressions and finite automata 
-/
import tactic

universes a b
-- α → Sigma,  q → state
-- Sigma and state 
/-
* A finite set of states Q (`q`)
* A finite set of input symbols, the alphabet, Siga (`α`)
* `q` and `α` should be the finset therefore, state `[fintype α]` and `[fintype q]`
-/
variables {α : Type a} [fintype α] {q : Type b} [fintype q] 
/-
when do the translating from NFA to DFA using the `subset construction` the states of DFA
should the subset of NFA which should be `set (q) ` which is the same as `q → Prop` in lean
-/
variable [fintype (q → Prop)]

/-
definition of word and language in `formal languages`
-/
section basic
-- word 
@[reducible]
def word (α)  := list α
-- language
@[reducible]
def lang (α)  := word α → Prop
-- def lang (α)  := set (list α)
end basic

/-
section `DFA` : Deterministic finite automaton
-/
section DFA
/-
1. A finite set of states Q (`q`)
2. A finite set of input symbols, the alphabet, Sigma (`α`)
3. A transition function δ ∈ Q × Sigma → Q  (`δ`)
4. An initial state q0 ∈ Q (`init`)
5. A set of final states F ⊆ Q (`final`)
-/
structure dfa (α : Type a) ( q : Type b) [fintype α] [fintype q] :=
(δ : q → α → q)
(init : q)
(final: q → Prop)

/-
`dfa_δ_star` evaluates `A` with input `w` starting from the state `q`.
`dfa_δ_star q w` gives the state after reading `w` starting from `q`
-/
def dfa_δ_star  (A : dfa α q) : q → word α → q
| q [] := q
| q (x :: w) := dfa_δ_star (A.δ q x) w

/-
`dfa_lang` is the language can be accepted by `A`
which means `dfa_δ_star A A.init w` is an final state
-/
def dfa_lang (A : dfa α q): lang α
| w := A.final (dfa_δ_star A A.init w)

end DFA

/-
section `NFA` : Nondeterministic finite automaton
-/
section NFA
/-
1. A finite set of states Q (`q`)
2. A finite set of input symbols, the alphabet, Sigma (`α`)
3. A transition function δ ∈ Q × Sigma → Q  (`δ`)
4. A set of initial states S ⊆ Q,(`inits`)
5. A set of final (or accepting) states F ⊆ Q. (`final`)
-/
structure nfa (α : Type a) ( q : Type b) [fintype α] [fintype q]  :=
(inits : q → Prop)
(final : q → Prop)
(δ : q → α → q → Prop)

/-
`nfa_δ_star` computes all possible paths though `B` with input `w` starting from the state `q`.
`nfa_δ_star q w` gives the set of possible states after reading `w` starting from `q`
-/
def nfa_δ_star (B : nfa α q) : q → word α → q → Prop
| q1 [] q2 := q1 = q2
| q1 (a::l) q2 := ∃ q3: q , B.δ q1 a q3 ∧ nfa_δ_star q3 l q2

-- -- try with decidable
-- instance (B : nfa α q) (q1 q2 : q) [decidable_eq q] : decidable (nfa_δ_star B q1 list.nil q2):=
-- begin
--   dsimp[nfa_δ_star],
--   exact decidable_eq _,

-- end
-- instance (B : nfa α q) [decidable_eq q] [Π q1 a q3, decidable (B.δ q1 a q3)] :
--   Π q1 a q2, decidable (nfa_δ_star B q1 a q2)
-- | q1 [] q2 := by apply_instance
-- | q1 (a :: l) q2 := fintype.decidable_exists_fintype
/-
`dfa_lang` is the language can be accepted by `B`
which means there is a final state in the set of state `nfa_δ_star B q w`.
-/
def nfa_lang (B : nfa α q) : lang α
|word := ∃q1 q2: q, B.inits q1 ∧ B.final q2 ∧ nfa_δ_star B q1 word q2
-------
end NFA

/-
section `DFA_to_NFA` shows the step about how to build an equavalent NFA using an existed DFA
-/
section DFA_to_NFA

/-
`d2n_init` translating the initial state from DFA to NFA
-/
def d2n_init(A : dfa α q): q → Prop
| q := A.init = q 
/-
`d2n_transition` translating the transition function from DFA to NFA
-/
def d2n_transition (A: dfa α q) : q → α → q → Prop
|q1 a q2 := (A.δ q1 a) = q2

/-
`dfa2nfa` construct an NFA using the existed DFA.
-/
def dfa2nfa : dfa α q → nfa α q
| dfa := { 
           inits := d2n_init dfa,
           final := dfa.final,
           δ := d2n_transition dfa
          } 

/-
* Following part in this section shows the proof:
for all DFA, there exist a NFA which can accept the same language with the DFA.
-/

lemma dfa_to_nfa_helper1 (A: dfa α q) (w : word α) : ∀ start : q, nfa_δ_star (dfa2nfa A) start w (dfa_δ_star A start w):=
begin
  induction w with c w' ih,
  {tauto},
  dsimp[nfa_δ_star, dfa_δ_star],
  assume s,
  existsi (A.δ s c),
  constructor,
  dsimp[dfa2nfa,d2n_transition],
  refl,
  apply ih,
end

lemma helper_2nd (A: dfa α q) (w : word α): ∀ start: q, (∃ q2 : q, (dfa2nfa A).final q2 ∧ nfa_δ_star (dfa2nfa A) start w q2) → A.final (dfa_δ_star A start w):=
begin
  induction w with  c w' ih,
  assume start,
  dsimp[dfa2nfa, nfa_δ_star, dfa_δ_star],
  assume m,
  cases m with q2 m,
  cases m with m1 m2,
  rw m2,
  exact m1,
  assume start,
  dsimp[nfa_δ_star, dfa_δ_star],
  assume m,
  apply ih,
  cases m with q2 m,
  cases m with m1 m2,
  cases m2 with q3 m2,
  cases m2 with m2 m3,
  existsi q2,
  constructor,
  exact m1,
  dsimp[dfa2nfa, d2n_transition] at m2,
  rw m2,
  exact m3,
end


lemma dfa_to_nfa_helper2 (A: dfa α q) (w : word α): nfa_lang (dfa2nfa A) w → dfa_lang A w:=
begin
  assume lang,
  dsimp[nfa_lang, dfa_lang] at *,  
  cases lang with q1 h,
  cases h with q2 h,
  cases h with h1 h2,
  cases h2 with h2 h3,
  dsimp [dfa2nfa, d2n_init] at h1,
  dsimp [dfa2nfa, d2n_transition] at h2,
  apply helper_2nd,
  dsimp[dfa2nfa],
  existsi q2,
  constructor,
  exact h2,
  rw h1,
  exact h3,
end

theorem dfa_to_nfa_1: ∀ dfa: dfa α q,  (∀ w : word α,  dfa_lang dfa w ↔ nfa_lang (dfa2nfa dfa) w):=
begin
  assume A,
  assume w,
  constructor,
  assume lang,
  dsimp [ nfa_lang, dfa_lang] at *,
  existsi A.init,
  existsi (dfa_δ_star A A.init w),
  constructor,
  dsimp[dfa2nfa, d2n_init],
  refl,
  constructor,
  exact lang,
  induction w with c w' ih gerializing A.init,
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  refl,
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  existsi (A.δ A.init c),
  constructor,
  dsimp [dfa2nfa, d2n_transition],
  refl,
  apply dfa_to_nfa_helper1,
  --------------
  apply dfa_to_nfa_helper2,
end

theorem dfa_to_nfa: ∀ dfa: dfa α q, ∀ w : word α, 
 dfa_lang dfa w = nfa_lang (dfa2nfa dfa) w:=
begin
  assume A w,
  ext x,
  apply dfa_to_nfa_1,
end
end DFA_to_NFA
-- ⊢ dfa_lang A w ↔ nfa_lang (dfa2nfa A) w



/-
section `NFA_to_DFA` shows the step about how to build an equavalent DFA using an existed NFA
using `subset construction`
-/

section NFA_to_DFA

/-
`n2d_final` translating the final state from NFA to DFA.

-/
def n2d_final (B : nfa α q) : (q → Prop) → Prop
| sub_q := ∃ q : q,  sub_q q ∧ B.final q 

/-
`n2d_transition` translating the transition function from DFA to NFA
-/
def n2d_transition (B : nfa α q) : (q → Prop) → α → q → Prop
| sub_q c q2 := ∃ q1 : q, sub_q q1 ∧ B.δ q1 c q2 

/-
`nfa2dfa` construct a DFA using the existed NFA. by `subset contruction`
The basic idea of this construction is to define a DFA whose states are sets of NFA states.
Therefore the state of DFA should be (`q → Prop`)
-/
def nfa2dfa : nfa α q → dfa α (q → Prop)
| B := {
        init := B.inits, -- 
        final := n2d_final B, 
        δ := n2d_transition B, 
        }

/-
* Following part in this section shows the proof:
for all NFA, there must exist a DFA which can accept the same language with the given NFA.
-/

lemma n2d_helper_1(B: nfa α q) (w : word α)  :∀ start: q → Prop,  (nfa2dfa B).final (dfa_δ_star (nfa2dfa B) start w) →  (∃ (q1 q2 : q), start q1 ∧ B.final q2 ∧ nfa_δ_star B q1 w q2):=
begin
  induction w with c w' ih,
  assume start,
  dsimp[dfa_δ_star, nfa_δ_star, nfa2dfa, n2d_final],
  assume m,
  cases m with q m,
  cases m with m1 m2,
  existsi q, existsi q,
  {tauto},
  assume start,
  -- (∃ (q1 q2 : q), start q1 ∧ B.final q2 ∧ nfa_δ_star B q1 (c :: w') q2)
  -- (∃ (q1 q2 : q), (n2d_transition B start c) q1 ∧ B.final q2 ∧ nfa_δ_star B q1 w' q2
  dsimp[dfa_δ_star, nfa_δ_star, nfa2dfa, n2d_final, n2d_transition] at *,
  assume m,
  have h : ∃ (q1 q2 : q), (n2d_transition B start c) q1 ∧ B.final q2 ∧ nfa_δ_star B q1 w' q2,
  apply ih,
  exact m,
  dsimp[n2d_transition] at h,
  cases h with q1 h, cases h with q2 h,
  cases h with h1 h2,
  cases h1 with q3 h3,
  cases h3 with h3 h4,
  cases h2 with h2 h5,
  existsi q3,
  existsi q2,
  -- constructor, exact h3, constructor, exact h2,
  -- existsi q1,
  -- constructor, exact h4, exact h5,
  {tauto},
end

lemma n2d_helper_2(B: nfa α q) (w : word α) : ∀(q2 : q),  ∀ start: q → Prop, (∃(q1 : q),  start q1 ∧ nfa_δ_star B q1 w q2 )→ dfa_δ_star (nfa2dfa B) start w q2:=
begin
  assume q2,
  induction w with c w' ih,
  assume start,
  assume m,
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  cases m with q1 m,
  cases m with st_q1 eq,
  rw← eq,
  exact st_q1,
  ----------
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  assume start,  
  assume m,
  cases m with q1 m,
  cases m with st_q1 m,
  cases m with q3 m,
  cases m with q1_c_q3 nfa_q3_w'_q2,
  apply ih,
  existsi q3,
  dsimp[nfa2dfa, n2d_transition],
  {tauto},
  -- constructor,
  -- existsi q1,
  -- constructor,
  -- exact st_q1,
  -- exact q1_c_q3,
  -- exact nfa_q3_w'_q2,
end

theorem nfa_to_dfa_1 : ∀ nfa: nfa α q,  (∀ w : word α,  dfa_lang (nfa2dfa nfa) w ↔ nfa_lang nfa w):=
begin
  assume B,
  assume w,
  constructor,
  assume lang,
  induction w with c w' ih,
  dsimp [ nfa_lang, dfa_lang] at *,
  dsimp [nfa2dfa, n2d_final, dfa_δ_star, nfa_δ_star] at *,
  cases lang with q h,
  cases h with h1 h2,
  existsi q, existsi q,
  {tauto},
  --------
  dsimp [ nfa_lang, dfa_lang] at *,
  -- dsimp [nfa_δ_star, dfa_δ_star],
  apply n2d_helper_1,
  exact lang,

  assume lang,
  dsimp [ nfa_lang, dfa_lang] at *,
  dsimp [nfa2dfa, n2d_final],
  cases lang with q1 h,
  cases h with q2 h,
  cases h with h1 h2,
  cases h2 with h2 h3,
  existsi q2,
  constructor,
  induction w with c w' ih,
  dsimp[dfa_δ_star, nfa_δ_star] at *,
  rw← h3, exact h1,
  dsimp[nfa_δ_star] at h3,
  dsimp[dfa_δ_star],
  cases h3 with q3 h,
  cases h with h3 h4,
  apply n2d_helper_2,
  dsimp[n2d_transition],
  existsi q3,
  {tauto},
  {tauto},
  -- constructor,
  -- existsi q1,
  -- constructor,
  -- exact h1, exact h3, exact h4,
  -- exact h2,
end

theorem nfa_to_dfa : ∀ nfa : nfa α q, ∀ w : word α, 
 dfa_lang (nfa2dfa nfa) w = nfa_lang nfa w:=
begin
  assume A w,
  ext x,
  apply nfa_to_dfa_1,
end

end NFA_to_DFA


/-
* section `regular_expression` contains the content of regular expression
* The `language` of Regular Expression
* The `syntax` of Regular Expression
-/
section regular_expression
/- 
`syntax`
1. ∅ is a regular expression. (empty set)
2. ε is a regular expression. (empty string)
3. For each x ∈ α , x is a regular expression. (literal character)
4. If E and F are regular expressions then E + F is a regular expression. 
5. If E and F are regular expressions then EF (juxtapositioning; just one
after the other) is a regular expression. (concatenation)
6. If E is a regular expression then E∗ is a regular expression. (Kleene star) 
7. If E is a regular expression then (E) is a regular expression
-/
inductive regular_expression (α : Type a)  : Type a
| empty : regular_expression
| epsilon : regular_expression
| char : α → regular_expression
| plus : regular_expression → regular_expression → regular_expression
| comp : regular_expression → regular_expression → regular_expression
| star : regular_expression → regular_expression
| bracket : regular_expression → regular_expression

open regular_expression

/-
`instance` of regular expression
1. using `+` to replace plus. 
2. using `*` to replace juxtapositioning.
-/
---
instance : has_add (regular_expression α) := ⟨plus⟩
lemma check_re_plus (P Q : regular_expression α) : plus P Q = P + Q := 
begin
  refl,
end

instance : has_mul (regular_expression α) := ⟨comp⟩
lemma check_re_mul (P Q : regular_expression α) : comp P Q = P * Q := 
begin
  refl,
end

/-- The image of a binary function `f : α → β → γ` as a function `set α → set β → set γ`.
  Mathematically this should be thought of as the image of the corresponding function `α × β → γ`.
-/
-- def image2 (f : α → β → γ) (s : set α) (t : set β) : set γ :=
-- {c | ∃ a b, a ∈ s ∧ b ∈ t ∧ f a b = c }
/-
`instance` of language
1. using `+` to replace Union
2. using `*` to replace image2 (++) l m
-/
instance : has_add (lang α) := ⟨set.union⟩
lemma check_lan_add (l m : lang α) : l + m = set.union l m := 
begin
  refl,
end

instance : has_mul (lang α) := ⟨set.image2 (++)⟩
lemma check_lan_mul (l m : lang α) : l * m = set.image2 (++) l m := 
begin
  refl,
end

/-
* (empty set) 
`∅` denoting the set `∅`.
Therefore, no words in the set
-/

def empty_check: lang α 
| w := false

/-
* (empty string) check if the language accept empty string
`ε` denoting the set containing only the "empty" string, 
which has no characters at all.
-/
def epsilon_check : lang α
| [] := true
| (a::l) := false
/-
* (char)
a in `Σ` denoting the set containing only the character a.
-/
def char_check: α → lang α
| a w := w = [a]

/-
define the function `star_lang`
The star of a language `L` is the set of all strings which can be written by concatenating
strings from `L`. 
-/
def star_lang: lang α → lang α
| l w := ∃ S : list (list α), w = S.join ∧ ∀ y ∈ S , l y 

/-- `reg_lang P` returns a language which contains all strings that `P` accept-/
def reg_lang : regular_expression α → lang α
|empty := empty_check -- ∅
|epsilon := epsilon_check
|(char a) := (char_check a)
|(plus P Q) := (reg_lang P) + (reg_lang Q) -- reg_lang P ∪ reg_lang Q
|(comp P Q) := reg_lang P * reg_lang Q -- ++ (reg_lang P X reg_lang Q)
|(star P) := star_lang (reg_lang P)
|(bracket P) := reg_lang P

/-- `epsilon_match P x` is true if and only if `P` matches the empty string -/
def epsilon_match : regular_expression α → bool
| empty := ff
| epsilon := tt
| (char _) := ff
| (P + Q) := epsilon_match P ∨ epsilon_match Q
| (P * Q) := epsilon_match P ∧ epsilon_match Q
| (star P) := tt
| (bracket P) := epsilon_match P

variable [decidable_eq α] -- state that α must be decidable for the following part

/- 
`step P a` read a character in the word by `P`, and return the regular expression
aftering recognizing.
-/ 

def step : regular_expression α → α → regular_expression α
| empty a := empty
| epsilon a := empty
| (char a₁) a₂ := if (a₁ = a₂) then epsilon else empty
| (P + Q) a := step P a + step Q a  
| (P * Q) a :=  if epsilon_match P then step P a * Q + step Q a else step P a * Q
| (star P) a := step P a * star P
| (bracket P) a := step P a

/- 
`reg_match P` is true if and only if `P` can match word `x`. 
the last component of the list must be `[]`
-/
def reg_match : regular_expression α → word α → bool
| P [] := epsilon_match P
| P (a::as) := reg_match (step P a) as

/-
* following parts are some tests of the correctness of `reg_match` in regular expression
-/

/-
`empty_reg_match` accept no word
-/
lemma empty_reg_match (x : word α) : reg_match empty x = ff :=
begin
  induction x,
  dsimp[reg_match, epsilon_match],
  refl,
  dsimp[reg_match, epsilon_match, step],
  exact x_ih,
end

/-
`epsilon_reg_match` accept empty string
-/
lemma epsilon_reg_match (x : word α) : reg_match epsilon x ↔ x = [] :=
begin
  induction x with c l ih,
  dsimp[reg_match, epsilon_match, step, *],
  {tauto},
  dsimp[reg_match, epsilon_match, step, *],
  cases ih with ih_1 ih_2,
  constructor,
  assume h,
  rw empty_reg_match at h,
  cases h,
  assume h,
  cases h,
end

/-
`char_reg_match` accept the string containing only the character a
-/
lemma char_reg_match (a : α) (x : word α) : reg_match (char a) x ↔ x = [a] :=
begin
  cases x with _ x,
  {tauto},
  cases x,
  rw [reg_match, step],
  split_ifs; -- split if then else
  {tauto},
  rw [reg_match, step],
  split_ifs,
  rw epsilon_reg_match,
  {tauto},
  rw empty_reg_match,
  tauto,
end


--- TODO : other semantics
--- TODO : translating between finite automata and regular expression

end regular_expression
