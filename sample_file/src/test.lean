set_option pp.structure_projections false
-- set_option pp.implicit true
set_option trace.eqn_compiler.elim_match true

@[reducible]
def FinSet := ℕ

-- n = { 0 , 1 ,2 , .. , n-1 }
-- 4 = {0,1,2,3} , fmax 4 = 3

-- A → B
-- Π (a : A) B a
-- Π {a : A} B a

open nat
-- Π
-- assuming α : Type is the first argument to the function
-- This is an instance of a Pi type, 
-- or dependent function type. Given α : Type and β : α → Type,
-- think of β as a family of types over α, that is, 
-- a type β a for each a : α. 
-- In that case, the type Π x : α, β x 
-- denotes the type of functions f with the property that, 
-- for each a : α, f a is an element of β a.

-- This definition is called inductive family
-- https://leanprover.github.io/theorem_proving_in_lean/inductive_types.html#inductive-families
-- FinSet is indices here

-- one of the element or all of the elements.
-- two kinds of contstructors


-- means produce
-- only construct El (succ n), which represent a natural number
-- EL is a type with two constructors fzero and fsucc
inductive El : FinSet → Type
| fzero : Π {n : FinSet}, El (succ n)
| fsucc : Π {n : FinSet}, El n → El (succ n)

#reduce El 0
#reduce El 1
#reduce El 2



--  El 2 = {fzero {1} , fsucc {1}(fzero {0})}
/-
El 0 = {}
El 1 = {fzero {0}}
El 2 = {fzero {1} , fsucc {1}(fzero {0})}
El 3 = {fzero {2}, fsucc {2} (fzero{1}) , fsucc {2} (fsucc {1}(zero {0}))
El n+1 = {fzero {n} , fsucc i | i in El n}
-/

open El


def fin2nat : Π (n : FinSet), El n → ℕ
| (succ n) fzero := zero
| (succ n) (fsucc i) := succ (fin2nat n i)


def fmax : Π (n : FinSet), El (succ n)
| zero := fzero
| (succ n) := fsucc (fmax n)

-- def last_element (n : FinSet): ℕ  :=  (fin2nat n (fmax (n-1)))


#check fmax 2
#check fmax 5
#reduce fmax 0
#reduce fmax 3
-- #check (fin2nat n (fmax (n-1)))
#check (fin2nat 4 (fmax 3))
#eval (fin2nat 1 fzero)
#eval (fin2nat 4 (fmax 3))
-- #eval (fin2nat 5 (fmax 3))
#eval (fin2nat 4 fzero.fsucc.fsucc.fsucc)
-- #eval (fin2nat 4 fmax 3)
#eval (fin2nat 4 fzero)
-- 4 = {0,1,2,3}
-- 3 = {0,1,2}
-- fmax 2 = 1 (fzero.fsucc.fsucc)  why 1 == fzero.fsucc.fsucc
-- fmax 3 = 2 
#eval (fin2nat 3 (fmax 2))

#reduce fmax 4

#reduce (fin2nat 3 (fmax 2))


variable {Sigma : FinSet}

-- Σ = { 'a' , 'b' }
-- Σ = 2, 
-- a = fzero : El Sigma
-- b = fsucc fzero : El Sigma

structure DFA (Sigma : FinSet) :=
  (Q : FinSet)
  (init : El Q)
  (final : El Q → bool)
  (δ : El Q → El Sigma → El Q)


-- plenty of transition functions
-- structure state need?


#check fmax 2
#reduce fmax 2

-- def x : {n : ℕ // 4 ≤ n} := ⟨4, le_refl 4⟩

open list 

-- word 
def Word (Sigma : FinSet) := list (El Sigma)
-- Sigma
-- set (Word Sigma)

def Lang (Sigma : FinSet) := Word Sigma -> Prop

--  set is a type of Predicate Yeah
-- def Lang_2 (Sigma : FinSet) := set (Word Sigma)


def dfa_lang (Sigma : FinSet):= DFA Sigma → Lang Sigma 


def eval_dfa (Sigma : FinSet) := DFA Sigma → Word Sigma → bool



-- (∀ x : Word Sigma, dfa_lang x) → 
-- def dfa1_lang (Sigma : FinSet) := DFA Sigma → Lang Sigma 
#reduce cons 1 (cons 2 (cons 3 nil))
#reduce cons 2 [2,3]
namespace dfa_1 

def Sigma1 : FinSet 
  := 2
def a : El Sigma1 
  := fzero
def b : El Sigma1
  := fsucc fzero

def Q1 : FinSet := 2
-- 2 = {fzero, fsucc fzero}
def q0 : El Q1 := fzero
def q1 : El Q1 := fsucc fzero

def final1 : El Q1 → bool
| fzero := false 
| (fsucc fzero) := true
-- #check final1

def word_1: Word Sigma1 := [fzero, fzero.fsucc, fzero]
-- aba
def word_2: Word Sigma1 := [fzero, fzero, fzero]
-- aaa
def word_3: Word Sigma1 := [fzero, fzero]
-- aa
def word_4: Word Sigma1 := [fzero]
-- a
def word_5: Word Sigma1 := [fzero.fsucc, fzero]
-- ba
def word_6: Word Sigma1 := [fzero.fsucc, fzero, fzero.fsucc]
-- bab


#reduce 3
-- def Lang_1: Lang Sigma1 := ∀ x : Word Sigma1, last character is 2


-- language of this 
-- def Lang_1 : Prop := (Lang Sigma1) word_1
#check Lang Sigma1
#check Lang Sigma1

def δ1 : El Q1 → El Sigma1 → El Q1
--| q0 a := q1
| fzero fzero := q1
--| q0 b := q0
| fzero (fsucc fzero) := q0
--| q1 a := q1  
| (fsucc fzero) fzero := q1
--| q1 b := q0
| (fsucc fzero) (fsucc fzero) := q0
-- L = {a, ba, aab, aba, aaba, abba, .....}


def dfa_a : DFA Sigma1 := 
  {Q := Q1 , 
   init := q0, 
   final := final1,
   δ := δ1}

-- def dfa1_lang: dfa_lang Sigma1 := (∀ x : Word Sigma1, Lang x)

-- def dfa1_Lang : Lang Sigma1 := 

-- def last_element(Sigma :FinSet) :Word Sigma1
-- the language of words that end with an “a”.
-- variables PP QQ : Word Sigma1 → Prop

-- ∀ x : Word Sigma
-- define the head of the Word

def hd : Word Sigma → Word Sigma
| [] := []
| (a :: l) := [a]

-- | [fzero] := true
-- | [fsucc fzero] := false

def snoc : list (El Q1) → El Q1 → list (El Q1)
| [] a := [a]
| (a :: as) b := a :: (snoc as b)

def rev : list (El Q1) → list (El Q1)
| [] := []
| (a :: as) := snoc (rev as) a

def is_dfa_a : Lang Sigma1
| nil := false
| (fzero :: _ ):= true
| (fsucc fzero :: _ ) := false

-- def is_dfa_a_2 : Lang Sigma1
-- | nil := false
-- | [fzero] := true
-- | [fsucc fzero] := false
-- | ( _ :: fzero::[] ):= true
-- | ( _ :: fsucc fzero :: [] ) := false

def is_dfa_a_3: Lang Sigma1
| nil := false
| [fzero] := true
| [fsucc fzero] := false
| (a::l) := is_dfa_a_3 l 



def dfa_lang (Sigma : FinSet) := DFA Sigma → Lang Sigma

-- def dfa_1_lang : dfa_lang Sigma1 := dfa_a → is_a

#check dfa_a
#check is_dfa_a
-- word_5: Word Sigma1 := [fzero.fsucc, fzero]------ ba
#reduce is_dfa_a (rev word_5) -- true
-- word_6: Word Sigma1 := [fzero.fsucc, fzero, fzero.fsucc] ------ bab
#reduce is_dfa_a (rev word_6) -- false



#reduce is_dfa_a_3 word_5


-- def word_1: Word Sigma1 := [fzero, fzero.fsucc, fzero]
-- -- aba
-- def word_2: Word Sigma1 := [fzero, fzero, fzero]
-- -- aaa
-- def word_3: Word Sigma1 := [fzero, fzero]
-- -- aa
-- def word_4: Word Sigma1 := [fzero]
-- -- a
-- def word_5: Word Sigma1 := [fzero.fsucc, fzero]
-- -- ba
-- def word_6: Word Sigma1 := [fzero.fsucc, fzero, fzero.fsucc]
-- -- bab
-- def dfa_2_lang (Sigma : FinSet): Word Sigma1 → Prop
-- | [] := false
-- | word_1 := true


-- #reduce dfa_2_lang Sigma1 




-- def eval_dfa: DFA Sigma → FinSet → bool





end dfa_1


namespace test

structure point (α : Type*) :=
(x : α) (y : α) (z : α)

structure rgb_val :=
(red : nat) (green : nat) (blue : nat)

structure red_green_point (α : Type*) extends point α, rgb_val :=
(no_blue : blue = 0)

def p   : point nat := {x := 10, y := 10, z := 20}
def rgp : red_green_point nat :=
{red := 200, green := 40, blue := 0, no_blue := rfl, ..p}

#reduce rgp

example : rgp.x   = 10 := rfl
example : rgp.red = 200 := rfl

 variables P Q R : Prop
 variables A B C : Type

-- BEGIN

 variables PP QQ : A → Prop
 -- END
 #check ∀ x : A, PP x ∧ Q
 #check (∀ x : A , PP x) ∧ Q
 #check ∀ x:A , (∃ x : A , PP x) ∧ QQ x
 #check ∀ y:A , (∃ z : A , PP z) ∧ QQ y

 
end test













-- define some automata (from the lecture notes)




-- define the language of an automata


