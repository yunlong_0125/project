set_option pp.structure_projections false
-- set_option pp.implicit true
set_option trace.eqn_compiler.elim_match true


@[reducible]
def FinSet := ℕ
-- n = { 0 , 1 ,2 , .. , n-1 }


-- 4 = {0,1,2,3} , fmax 4 = 3

-- A → B
-- Π (a : A) B a
-- Π {a : A} B a

open nat
-- Π
-- assuming α : Type is the first argument to the function
-- This is an instance of a Pi type, 
-- or dependent function type. Given α : Type and β : α → Type,
-- think of β as a family of types over α, that is, 
-- a type β a for each a : α. 
-- In that case, the type Π x : α, β x 
-- denotes the type of functions f with the property that, 
-- for each a : α, f a is an element of β a.

-- This definition is called inductive family
-- https://leanprover.github.io/theorem_proving_in_lean/inductive_types.html#inductive-families
-- FinSet is indices here

-- one of the element or all of the elements.
-- two kinds of contstructors


-- means produce

-- only construct El (succ n), which represent a natural number


-- EL is a type with two constructors fzero and fsucc
inductive El : FinSet → Type
| fzero : Π {n : FinSet}, El (succ n)
| fsucc : Π {n : FinSet}, El n → El (succ n)

/-
El 0 = {}
El 1 = {fzero {0}}
El 2 = {fzero {1} , fsucc {1}(fzero {0})}
El 3 = {fzero {2}, fsucc {2} (fzero{1}) , fsucc {2} (fsucc {1}(zero {0}))
El n+1 = {fzero {n} , fsucc i | i in El n}
-/

open El

def fin2nat : Π (n : FinSet), El n → ℕ
| (succ n) fzero := zero
| (succ n) (fsucc i) := succ (fin2nat n i)


def fmax : Π (n : FinSet), El (succ n)
| zero := fzero
| (succ n) := fsucc (fmax n)

-- def last_element (n : FinSet): ℕ  :=  (fin2nat n (fmax (n-1)))


#check fmax 2
#check fmax 5
#reduce fmax 0
#reduce fmax 3
-- #check (fin2nat n (fmax (n-1)))
#check (fin2nat 4 (fmax 3))
#eval (fin2nat 1 fzero)
#eval (fin2nat 4 (fmax 3))
-- #eval (fin2nat 5 (fmax 3))
#eval (fin2nat 4 fzero.fsucc.fsucc.fsucc)
-- #eval (fin2nat 4 fmax 3)
#eval (fin2nat 4 fzero) -- 0
-- 4 = {0,1,2,3}
-- 3 = {0,1,2}
-- fmax 2 = 1 (fzero.fsucc.fsucc)  why 1 == fzero.fsucc.fsucc
-- fmax 3 = 2 
#eval (fin2nat 3 (fmax 2))

#reduce fmax 4 = fzero.fsucc.fsucc.fsucc.fsucc

#reduce (fin2nat 3 (fmax 2))


variable {Sigma : FinSet}

-- Σ = { 'a' , 'b' }
-- Σ = 2, 
-- a = fzero : El Sigma
-- b = fsucc fzero : El Sigma

structure DFA (Sigma : FinSet) :=
  (Q : FinSet)
  (init : El Q)
  (final : El Q → bool)
  (δ : El Q → El Sigma → El Q)




#check fmax 2
#reduce fmax 2

-- def x : {n : ℕ // 4 ≤ n} := ⟨4, le_refl 4⟩

open list 

-- word 
def Word (Sigma : FinSet) := list (El Sigma)

-- language
def Lang (Sigma : FinSet) := Word Sigma -> Prop -- set (Word Sigma)



--  set is a type of Predicate

-- def Lang_2 (Sigma : FinSet) := set (Word Sigma)


def delta_dfa (dfa : DFA Sigma): El dfa.Q → Word Sigma → El dfa.Q
| q [] := q -- return the final state of this word
| q (a::l) := delta_dfa (dfa.δ q a) l  -- transform from one state to another state receive all words in the DFA

def DFA_Lang : DFA Sigma → Lang Sigma
| dfa word := dfa.final (delta_dfa dfa dfa.init word)

-- namespace dfa_1 

-- def Sigma1 : FinSet 
--   := 2
-- def a : El Sigma1 
--   := fzero
-- def b : El Sigma1
--   := fsucc fzero

-- def Q1 : FinSet := 2
-- -- 2 = {fzero, fsucc fzero}
-- def q0 : El Q1 := fzero
-- def q1 : El Q1 := fsucc fzero

-- def final1 : El Q1 → bool
-- | fzero := false 
-- | (fsucc fzero) := true
-- -- #check final1

-- def word_1: Word Sigma1 := [fzero, fzero.fsucc, fzero]
-- -- aba
-- def word_2: Word Sigma1 := [fzero, fzero, fzero]
-- -- aaa
-- def word_3: Word Sigma1 := [fzero, fzero]
-- -- aa
-- def word_4: Word Sigma1 := [fzero]
-- -- a
-- def word_5: Word Sigma1 := [fzero.fsucc, fzero]
-- -- ba
-- def word_6: Word Sigma1 := [fzero.fsucc, fzero, fzero.fsucc]
-- -- bab
-- def word_7: Word Sigma1 := [fzero.fsucc]
-- -- b


-- #reduce 3
-- -- def Lang_1: Lang Sigma1 := ∀ x : Word Sigma1, last character is 2


-- -- language of this 
-- -- def Lang_1 : Prop := (Lang Sigma1) word_1
-- #check Lang Sigma1

-- def δ1 : El Q1 → El Sigma1 → El Q1
-- --| q0 a := q1
-- | fzero fzero := q1
-- --| q0 b := q0
-- | fzero (fsucc fzero) := q0
-- --| q1 a := q1  
-- | (fsucc fzero) fzero := q1
-- --| q1 b := q0
-- | (fsucc fzero) (fsucc fzero) := q0
-- -- L = {a, ba, aab, aba, aaba, abba, .....}

-- def dfa_a : DFA Sigma1 := 
--   {Q := Q1 , 
--    init := q0, 
--    final := final1,
--    δ := δ1}

-- def is_dfa_a: Lang Sigma1
-- | nil := ff
-- | [fzero] := tt
-- | [fsucc fzero] := ff -- dont need this line
-- | (a::l) := is_dfa_a l 

-- def delta_dfa_a : El Q1 → Word Sigma1 → El Q1
-- | q [] := q
-- | q (a::l) := delta_dfa_a (dfa_a.δ q a) l  -- transform from one state to another state receive all words in the DFA



-- def eval_dfa_a : Word Sigma1 → El Q1  → Prop 
-- | nil q := dfa_a.final q
-- | (a::l) q := eval_dfa_a l (dfa_a.δ q a)


-- #reduce eval_dfa_a [a,b,a] dfa_a.init



-- #check dfa_a
-- #reduce dfa_a

-- #reduce is_dfa_a word_1 -- true -- aba
-- #reduce is_dfa_a word_2 -- true -- aaa
-- #reduce is_dfa_a word_3 -- true -- aa
-- #reduce is_dfa_a word_4 -- true -- a
-- #reduce is_dfa_a word_5 -- true -- ba
-- #reduce is_dfa_a word_6 -- false -- bab
-- #reduce is_dfa_a word_7 -- false -- b

-- -- #reduce DFA_Lang dfa_a word_6

-- -- def word_1: Word Sigma1 := [fzero, fzero.fsucc, fzero]
-- -- -- aba
-- -- def word_2: Word Sigma1 := [fzero, fzero, fzero]
-- -- -- aaa
-- -- def word_3: Word Sigma1 := [fzero, fzero]
-- -- -- aa
-- -- def word_4: Word Sigma1 := [fzero]
-- -- -- a
-- -- def word_5: Word Sigma1 := [fzero.fsucc, fzero]
-- -- -- ba
-- -- def word_6: Word Sigma1 := [fzero.fsucc, fzero, fzero.fsucc]
-- -- -- bab
-- -- def word_7: Word Sigma1 := [fzero.fsucc]
-- -- -- b

-- end dfa_1

-- helper

def equal_nat : ℕ → ℕ → ℕ
|zero zero := 1
|zero _ := 0
|_ zero :=0
|(succ n) (succ m) := equal_nat n m

def len: Word Sigma → ℕ
| [] := 0
| (a::l) := 1 + len l

def el_Eq: El Sigma → El Sigma → ℕ
|a b:= equal_nat (fin2nat Sigma a) (fin2nat Sigma b)

def counter_el : El Sigma → Word Sigma → ℕ 
| k [] := 0
| k (a::l) := (el_Eq k a) + counter_el k l

def nat2bool: ℕ  → bool
| zero := ff
| 1 := tt
| _ := tt

def bool_xor : bool → bool → bool
|tt ff := ff
|ff tt := ff
|tt tt := tt
|ff ff := tt

def bool_and : bool → bool → bool
|tt tt := tt
|ff ff := ff
|tt ff := ff
|ff tt := ff

-- namespace mod_dfa



-- -- def Word (Sigma : FinSet) := list (El Sigma)
-- -- def Lang (Sigma : FinSet) := Word Sigma -> Prop
-- -- #reduce 2 % 2
-- -- #reduce [1,2,3,4].length

-- #reduce fin2nat 3 (fsucc fzero)
-- def Sigma1 : FinSet := 2
-- def Q1 : FinSet := 2
-- def δ1 : El Q1 → El Sigma1 → El Q1
-- | fzero fzero := fsucc fzero
-- | fzero (fsucc fzero) := fsucc fzero
-- | (fsucc fzero) fzero :=  fzero
-- | (fsucc fzero) (fsucc fzero) := fzero
-- def init1 : El Q1 := fzero
-- def final1 : El Q1 → bool
-- | fzero := tt
-- | (fsucc fzero) := ff 
-- def dfa : DFA Sigma1 := {
--    Q := Q1 ,
--    init := init1, 
--    final := final1,
--    δ := δ1}


-- -- #reduce el_Eq fzero (fsucc fzero)

-- def word1 : Word Sigma1 := [fzero, fsucc fzero, fzero, fsucc fzero] -- abab
-- def word2 : Word Sigma1 := [fzero, fsucc fzero, fzero, fsucc fzero, fzero] -- ababa
-- def word3 : Word Sigma1 := [fzero, fsucc fzero, fzero, fsucc fzero, fsucc fzero] -- ababb
-- def word4 : Word Sigma1 := [fzero, fsucc fzero, fzero, fsucc fzero, fsucc fzero, fzero] -- ababba
-- def word5 : Word Sigma1 := [fzero, fzero] -- aa
-- def word6 : Word Sigma1 := [fzero, fzero, fzero] -- aaa
-- def word7 : Word Sigma1 := [fzero, fzero, fsucc fzero]

-- -- #reduce counter_el fzero word4
-- #reduce counter_el (fsucc fzero) word4
-- #reduce counter_el fzero word3

-- --   (Q : FinSet)
-- --   (init : El Q)
-- --   (final : El Q → bool)
-- --   (δ : El Q → El Sigma → El Q)

-- def counter_el : El Sigma → Word Sigma → ℕ 
-- | k [] := 0
-- | k (a::l) := (el_Eq k a) + counter_el k l

-- def is_even: ℕ → bool
-- | n := if n % 2 = 0 then tt else ff

-- def is_odd: ℕ → bool
-- | n := if n % 2 = 1 then tt else ff

-- def mod_dfa_lang: Lang Sigma1
-- | word := 
-- (is_even (counter_el fzero word) ∧ is_even (counter_el (fsucc fzero) word)) ∨ 
-- (is_odd (counter_el fzero word) ∧ is_odd (counter_el (fsucc fzero) word))

-- #reduce mod_dfa_lang word1
-- #reduce mod_dfa_lang word2
-- #reduce mod_dfa_lang word3
-- #reduce mod_dfa_lang word4 
-- #reduce mod_dfa_lang word5
-- #reduce mod_dfa_lang word6
-- #reduce mod_dfa_lang word7

-- #reduce DFA_Lang dfa word1
-- #reduce DFA_Lang dfa word2
-- #reduce DFA_Lang dfa word3
-- #reduce DFA_Lang dfa word4
-- #reduce DFA_Lang dfa word5
-- #reduce DFA_Lang dfa word6
-- #reduce DFA_Lang dfa word7

-- end mod_dfa





-- namespace dfa_2
-- -- structure DFA (Sigma : FinSet) :=
-- --   (Q : FinSet)
-- --   (init : El Q)
-- --   (final : El Q → bool)
-- --   (δ : El Q → El Sigma → El Q)
-- def Sigma2 : FinSet := 2
-- def Q2 : FinSet := 3
-- def final2 : El Q2 → bool
-- | fzero := ff
-- | (fsucc fzero) := ff
-- | (fsucc (fsucc fzero)) := tt

-- def δ2 : El Q2 → El Sigma2 → El Q2
-- | fzero fzero := fsucc fzero
-- | fzero (fsucc fzero) := (fsucc (fsucc fzero)) 
-- | (fsucc fzero) fzero := fsucc fzero
-- | (fsucc fzero) (fsucc fzero) := (fsucc (fsucc fzero)) 
-- | (fsucc (fsucc fzero))  fzero := (fsucc (fsucc fzero)) 
-- | (fsucc (fsucc fzero))  (fsucc fzero) := (fsucc (fsucc fzero)) 

-- def dfa_b : DFA Sigma2 := 
--   {Q := Q2 , 
--    init := fzero,
--    final := final2,
--    δ := δ2}

-- def is_b : El Sigma2 → Prop
-- | fzero := false
-- | (fsucc fzero) := true


-- def is_dfa_b : Lang Sigma2
-- | nil:= false
-- | (a::l) :=  is_b a ∨ is_dfa_b l 

-- def word_1: Word Sigma2 := [fzero, fzero.fsucc, fzero]
-- -- aba
-- def word_2: Word Sigma2 := [fzero, fzero, fzero]
-- -- aaa
-- def word_3: Word Sigma2 := [fzero, fzero]
-- -- aa
-- def word_4: Word Sigma2 := [fzero]
-- -- a
-- def word_5: Word Sigma2 := [fzero.fsucc, fzero]
-- -- ba
-- def word_6: Word Sigma2 := [fzero.fsucc, fzero, fzero.fsucc]
-- -- bab
-- def word_7: Word Sigma2 := [fzero.fsucc]
-- -- b
-- #reduce is_dfa_b word_1 -- true -- aba
-- #reduce is_dfa_b word_2 -- false -- aaa
-- #reduce is_dfa_b word_3 -- false -- aa
-- #reduce is_dfa_b word_4 -- false -- a
-- #reduce is_dfa_b word_5 -- true -- ba
-- #reduce is_dfa_b word_6 -- true -- bab
-- #reduce is_dfa_b word_7 -- true -- b

-- def eval_dfa_b : Word Sigma2 → El Q2  → Prop 
-- | nil q := dfa_b.final q
-- | (a::l) q := eval_dfa_b l (dfa_b.δ q a)

-- #reduce eval_dfa_b word_1 dfa_b.init-- true -- aba
-- #reduce eval_dfa_b word_2 dfa_b.init-- false -- aaa
-- #reduce eval_dfa_b word_3 dfa_b.init-- false -- aa
-- #reduce eval_dfa_b word_4 dfa_b.init-- false -- a
-- #reduce eval_dfa_b word_5 dfa_b.init-- true -- ba
-- #reduce eval_dfa_b word_6 dfa_b.init-- true -- bab
-- #reduce eval_dfa_b word_7 dfa_b.init-- true -- b

-- end dfa_2

-- namespace dfa_3

-- -- structure DFA (Sigma : FinSet) :=
-- --   (Q : FinSet)
-- --   (init : El Q)
-- --   (final : El Q → bool)
-- --   (δ : El Q → El Sigma → El Q)
-- def Sigma3 : FinSet := 2
-- def Q3 : FinSet := 4
-- def final3 : El Q3 → bool
-- | fzero := ff
-- | (fsucc fzero) := ff
-- | (fsucc (fsucc fzero)) := tt
-- | (fsucc(fsucc (fsucc fzero))) := ff

-- def δ3 : El Q3 → El Sigma3 → El Q3
-- | fzero fzero := fsucc fzero
-- | fzero (fsucc fzero) := fsucc fzero
-- | (fsucc fzero) fzero := (fsucc (fsucc fzero))
-- | (fsucc fzero) (fsucc fzero) := (fsucc (fsucc fzero)) 
-- | (fsucc (fsucc fzero))  fzero := fsucc(fsucc (fsucc fzero)) 
-- | (fsucc (fsucc fzero))  (fsucc fzero) := fsucc (fsucc (fsucc fzero)) 
-- | (fsucc (fsucc (fsucc fzero))) fzero :=  fsucc (fsucc (fsucc fzero)) 
-- | (fsucc (fsucc (fsucc fzero))) (fsucc fzero) :=  fsucc (fsucc (fsucc fzero)) 


-- def dfa_c : DFA Sigma3 := 
--   {Q := Q3 , 
--    init := fzero,
--    final := final3,
--    δ := δ3}


-- -- language consists of words whose length is 2
-- def is_dfa_c : Lang Sigma3
-- | nil := false
-- | [fzero, fzero] := true
-- | [fzero, fsucc fzero] := true
-- | [fsucc fzero, fzero] := true 
-- | [fsucc fzero, fsucc fzero] := true 
-- | (e :: l) := false


-- def word_1: Word Sigma3 := [fzero, fzero.fsucc, fzero]
-- -- aba
-- def word_2: Word Sigma3 := [fzero, fzero, fzero]
-- -- aaa
-- def word_3: Word Sigma3 := [fzero, fzero]
-- -- aa
-- def word_4: Word Sigma3 := [fzero]
-- -- a
-- def word_5: Word Sigma3 := [fzero.fsucc, fzero]
-- -- ba
-- def word_6: Word Sigma3 := [fzero.fsucc, fzero, fzero.fsucc]
-- -- bab
-- def word_7: Word Sigma3 := [fzero.fsucc]
-- -- b
-- #reduce is_dfa_c word_1 -- false -- aba
-- #reduce is_dfa_c word_2 -- false -- aaa
-- #reduce is_dfa_c word_3 -- true -- aa
-- #reduce is_dfa_c word_4 -- false -- a
-- #reduce is_dfa_c word_5 -- true -- ba
-- #reduce is_dfa_c word_6 -- false -- bab
-- #reduce is_dfa_c word_7 -- false -- b

-- def eval_dfa_c : Word Sigma3 → El Q3  → Prop 
-- | nil q := dfa_c.final q
-- | (a::l) q := eval_dfa_c l (dfa_c.δ q a)
-- ---not display
-- #reduce eval_dfa_c word_1 dfa_c.init-- false -- aba
-- #reduce eval_dfa_c word_2 dfa_c.init-- false -- aaa
-- #reduce eval_dfa_c word_3 dfa_c.init-- true -- aa
-- #reduce eval_dfa_c word_4 dfa_c.init-- false -- a
-- #reduce eval_dfa_c word_5 dfa_c.init-- true -- ba
-- #reduce eval_dfa_c word_6 dfa_c.init-- false -- bab
-- #reduce eval_dfa_c word_7 dfa_c.init-- false -- b


-- end dfa_3

-- namespace dfa_4

-- -- structure DFA (Sigma : FinSet) :=
-- --   (Q : FinSet)
-- --   (init : El Q)
-- --   (final : El Q → bool)
-- --   (δ : El Q → El Sigma → El Q)
-- def Sigma3 : FinSet := 2
-- def Q3 : FinSet := 5
-- def final3 : El Q3 → bool
-- | fzero := tt
-- | (fsucc fzero) := tt
-- | (fsucc (fsucc fzero)) := tt
-- | (fsucc(fsucc (fsucc fzero))) := tt
-- | (fsucc (fsucc(fsucc (fsucc fzero)))) := ff

-- def δ3 : El Q3 → El Sigma3 → El Q3
-- | fzero fzero := fsucc fzero
-- | fzero (fsucc fzero) := fzero
-- | (fsucc fzero) fzero := (fsucc (fsucc fzero)) 
-- | (fsucc fzero) (fsucc fzero) := fzero
-- | (fsucc (fsucc fzero))  fzero := (fsucc (fsucc fzero)) 
-- | (fsucc (fsucc fzero))  (fsucc fzero) := fsucc (fsucc (fsucc fzero)) 
-- | (fsucc (fsucc (fsucc fzero))) fzero :=  (fsucc fzero) 
-- | (fsucc (fsucc (fsucc fzero))) (fsucc fzero) :=  (fsucc (fsucc (fsucc (fsucc fzero)))) 
-- | (fsucc (fsucc (fsucc (fsucc fzero)))) fzero :=  (fsucc (fsucc (fsucc (fsucc fzero)))) 
-- | (fsucc (fsucc (fsucc (fsucc fzero)))) (fsucc fzero) :=  (fsucc (fsucc (fsucc (fsucc fzero)))) 


-- def dfa_c : DFA Sigma3 := 
--   {Q := Q3 , 
--    init := fzero,
--    final := final3,
--    δ := δ3}


-- -- cannot enumerate it, too complicated
-- -- def is_dfa_c : Lang Sigma3
-- -- | nil := false


-- def word_1: Word Sigma3 := [fzero, fzero.fsucc, fzero]
-- -- aba
-- def word_2: Word Sigma3 := [fzero, fzero, fzero]
-- -- aaa
-- def word_3: Word Sigma3 := [fzero, fzero]
-- -- aa
-- def word_4: Word Sigma3 := [fzero]
-- -- a
-- def word_5: Word Sigma3 := [fzero.fsucc, fzero]
-- -- ba
-- def word_6: Word Sigma3 := [fzero.fsucc, fzero, fzero.fsucc]
-- -- bab
-- def word_7: Word Sigma3 := [fzero.fsucc]
-- -- b
-- def word_8: Word Sigma3 := [fzero, fzero, fzero.fsucc, fzero.fsucc]
-- -- aabb

-- def eval_dfa_c : Word Sigma3 → El Q3  → Prop 
-- | nil q := dfa_c.final q
-- | (a::l) q := eval_dfa_c l (dfa_c.δ q a)

-- #reduce eval_dfa_c word_1 dfa_c.init-- true -- aba
-- #reduce eval_dfa_c word_2 dfa_c.init-- true -- aaa
-- #reduce eval_dfa_c word_3 dfa_c.init-- true -- aa
-- #reduce eval_dfa_c word_4 dfa_c.init-- true-- a
-- #reduce eval_dfa_c word_5 dfa_c.init-- true -- ba
-- #reduce eval_dfa_c word_6 dfa_c.init-- true -- bab
-- #reduce eval_dfa_c word_7 dfa_c.init-- true -- b
-- #reduce eval_dfa_c word_8 dfa_c.init-- false -- aabb


-- end dfa_4

-- structure DFA (Sigma : FinSet) :=
--   (Q : FinSet)
--   (init : El Q)
--   (final : El Q → bool)
--   (δ : El Q → El Sigma → El Q)
-- type DFA st = ([st], [Char], (st->Char->st), st, (st->Bool))

-- type NFA st = ([st], [Char], st->(Maybe Char)->[st], st, st->Bool) haskell
-- https://cseweb.ucsd.edu/classes/wi14/cse105-a/haskell/intro.html

-- difference
-- may not have an input for some states
-- may have multiple identical inputs leading to different

structure NFA (Sigma : FinSet) :=
  (Q : FinSet)  
  (init : El Q → bool)
  (final : El Q → bool)
  (δ : El Q → El Sigma → El Q → bool)


def delta_ext_NFA(nfa: NFA Sigma): El nfa.Q → Word Sigma → El nfa.Q → Prop
| q1 [] q2 := q1=q2
| q1 (a::l) q2 := ∃q3: El nfa.Q, nfa.δ q1 a q3 ∧ delta_ext_NFA q3 l q2

def NFA_Lang: NFA Sigma → Lang Sigma
| nfa word := ∃q1 q2: El nfa.Q, nfa.init q1 ∧ nfa.final q2 ∧ delta_ext_NFA nfa q1 word q2

-- def DFA_Lang : DFA Sigma → Lang Sigma
-- | dfa word := dfa.final (delta_dfa dfa dfa.init word)


-- structure DFA (Sigma : FinSet) :=
--   (Q : FinSet)
--   (init : El Q)
--   (final : El Q → bool)
--   (δ : El Q → El Sigma → El Q)

-- structure NFA (Sigma : FinSet) :=
--   (Q : FinSet)  
--   (init : El Q → bool)
--   (final : El Q → bool)
--   (δ : El Q → El Sigma → El Q → bool)



-- def dfa_same_state(dfa1 : DFA Sigma) (dfa2: DFA Sigma): El (DFA.Q dfa1) → El (DFA.Q dfa2) → bool
-- |q1 q2 := equal_nat_2 (fin2nat dfa1.Q q1) (fin2nat dfa2.Q q2)

-- def dfa_same_Q: DFA Sigma → DFA Sigma → bool
-- | dfa_1 dfa_2 := dfa_1.Q = dfa_2.Q

-- def dfa_same_init: DFA Sigma → DFA Sigma → bool
-- | dfa_1 dfa_2 := dfa_same_state dfa_1 dfa_2 dfa_1.init dfa_2.init

-- def dfa_same_final: DFA Sigma → DFA Sigma → bool
-- | dfa_1 dfa_2 := sorry

-- def dfa_same_transition: DFA Sigma → DFA Sigma → bool
-- | dfa_1 dfa_2 := sorry
def equal_nat_3 : ℕ → ℕ → bool
| x y :=  x = y 

def equal_nat_2 : ℕ → ℕ → bool
|zero zero := tt
|zero _ := ff
|_ zero := ff
|(succ n) (succ m) := equal_nat_2 n m

def same_state(dfa : DFA Sigma) :El (DFA.Q dfa) → El (DFA.Q dfa) → bool
|q1 q2 := equal_nat_2 (fin2nat dfa.Q q1) (fin2nat dfa.Q q2)

def d2n_init(dfa : DFA Sigma):El dfa.Q → bool
| q := if same_state dfa dfa.init q then tt else ff

def d2n_transition (dfa: DFA Sigma) : El dfa.Q → El Sigma → El dfa.Q → bool
|q1 a q2 := if same_state dfa (dfa.δ q1 a) q2 then tt else ff

def dfa2nfa : DFA Sigma → NFA Sigma
| dfa := { Q := dfa.Q,
           init := d2n_init dfa,
           final := dfa.final,
           δ := d2n_transition dfa
          } 


-- def dfa_nfa_recog : DFA Sigma → NFA Sigma → bool
-- | dfa nfa := if (DFA_Lang dfa = NFA_Lang nfa) then tt else ff


-- def eq_dfa_nfa(dfa : DFA Sigma)(nfa : NFA Sigma) : DFA Sigma → NFA Sigma → bool
-- | dfa nfa := dfa.Q = nfa.Q ∧ (nfa.init = d2n_init dfa) ∧ (d2n_transition dfa = dfa.δ ∧ dfa.final = nfa.final → DFA_Lang dfa = NFA_Lang nfa

theorem dfa_to_nfa: ∀ dfa: DFA Sigma, ∃ nfa : NFA Sigma,
(∀ w : Word Sigma,  DFA_Lang dfa w ↔ NFA_Lang nfa w):=
begin
  assume dfa_1,
  have nfa_1 : NFA Sigma,
  apply dfa2nfa,
  exact dfa_1,
  existsi nfa_1,

  sorry,
end

{
DFA_Lang dfa : word → Prop

propExt : (P Q : A → Prop) → ((a : A) → P a ↔ Q a) ↔ (P = Q)
???

(w : Word) → DFA_Lang dfa w ↔ NFA_Lang nfa w
→ :
DFA_Lang dfa w
dfa.final (delta_dfa dfa dfa.init w)
nfa.final (delta_dfa dfa dfa.init w)

q1 = dfa.init
nfa.init q1 = same_state q1 dfa_init
q2 = delta_dfa dfa dfa.init
-- you need that same_state x x, which follow from same_state x y ↔ x=y
-- you need a lemma:

? delta_ext_NFA (d2n_transition dfa.δ) q w (delta_dfa dfa q w)  
Lemma: you prove this by induction over w 


? : NFA_Lang nfa w
∃q1 q2: El nfa.Q, nfa.init q1 ∧ nfa.final q2 ∧ delta_ext_NFA nfa q1 word q2 


  -- def delta_dfa (dfa : DFA Sigma): El dfa.Q → Word Sigma → El dfa.Q
  -- | q nil := q -- return the final arrival state of this word
  -- | q (a::l) := delta_dfa (dfa.δ q a) l 

  -- def delta_ext_NFA(nfa: NFA Sigma): El nfa.Q → Word Sigma → El nfa.Q → Prop
  -- | q1 [] q2 := q1 = q2
  -- | q1 (a::l) q2 := ∃q3: El nfa.Q, nfa.δ q1 a q3 ∧ delta_ext_NFA q3 l q2

  -- w'
  -- q1 = (dfa.init)
  -- q2 = (delta_dfa dfa q1 w)
  -- q2 = (delta_dfa dfa q3 w')
  -- q2 = (delta_dfa dfa (dfa.δ q1 c) w')
  -- q3 = (dfa.δ q1 c)

  -- ih : dfa.final (delta_dfa dfa q1 w') → delta_ext_NFA nfa q1 w' (delta_dfa dfa q1 w')
  -- ih : if w = w' → delta_ext_NFA nfa q1 w' q2 is true
  -- dfa_lang : (dfa.final q2)
  -- goal : delta_ext_NFA nfa q1 w q2
  -- goal : nfa.δ (DFA.init dfa) c (DFA.δ dfa (DFA.init dfa) c) ∧ 
  -- delta_ext_NFA nfa q3 w' q2  is true
  -- exist q3, nfa.δ q1 c q3 
  -- delta_ext_NFA nfa q3 w' q2

← ...




-}





namespace nfa_1

def Sigma1 : FinSet := 2 -- {0,1}
def Q1 : FinSet := 5 -- {A, B, C, D, E}
def A : El Q1 := fzero
def B : El Q1 := fzero.fsucc
def C : El Q1 := fzero.fsucc.fsucc
def D : El Q1 := fzero.fsucc.fsucc.fsucc
def E : El Q1 := fzero.fsucc.fsucc.fsucc.fsucc

def final1 : El Q1 → bool
|fzero := ff
|(fsucc fzero) := ff
|(fsucc (fsucc fzero)) := ff
|(fsucc (fsucc (fsucc fzero))) := tt
|(fsucc (fsucc (fsucc (fsucc fzero)))) := tt


def init1 : El Q1 → bool
|fzero:= tt
|_ := ff

-- A → 0 →  A or C
-- build more than 1 
def δ1: El Q1 → El Sigma1 → El Q1 → bool
|fzero fzero fzero := tt                                                                    -- A → 0 → A 
|fzero fzero (fsucc fzero):= tt                                                             -- A → 0 → C  
|fzero (fsucc fzero) (fsucc fzero) := tt                                                    -- A → 1 → B
|fzero (fsucc fzero) (fsucc (fsucc (fsucc fzero))) := tt                                    -- A → 1 → D
|fzero (fsucc fzero) (fsucc (fsucc (fsucc (fsucc fzero)))):= tt                             -- A → 1 → E
|(fsucc (fsucc fzero)) fzero (fsucc (fsucc fzero)) := tt                                    -- C → 0 → C
|(fsucc (fsucc fzero)) (fsucc fzero) (fsucc (fsucc (fsucc fzero))) := tt                    -- C → 1 → D
|(fsucc (fsucc (fsucc fzero))) fzero (fsucc (fsucc (fsucc (fsucc fzero)))) :=tt             -- D → 0 → E
|_ _ _ := ff

#check δ1 fzero fzero


-- def is_nfa_1 : Lang Sigma1
-- | nil:= false


def word_1: Word Sigma1 := [fzero, fzero.fsucc] -- 01


-- def delta_dfa (dfa : DFA Sigma): El dfa.Q → Word Sigma → El dfa.Q
-- | q [] := q -- return the final state of this word
-- | q (a::l) := delta_dfa (dfa.δ q a) l  
-- transform from one state to another state receive all words in the DFA

def nfa_1 : NFA Sigma1 := 
  {Q := Q1 , 
   init :=init1,
   final := final1,
   δ := δ1}

end nfa_1

namespace nfa_beginwith10

def Sigma1 : FinSet := 2 -- {0, 1}
def Q1 : FinSet := 3 -- {A, B, C}
def A : El Q1 := fzero
def B : El Q1 := fzero.fsucc

def final1 : El Q1 → bool
|fzero := ff
|(fsucc fzero) := ff
|(fsucc (fsucc fzero)) := tt

def init1 : El Q1 → bool
|fzero:= tt
|(fsucc fzero) := ff
|(fsucc (fsucc fzero)) := ff

def δ1: El Q1 → El Sigma1 → El Q1 → bool
|fzero (fsucc fzero) (fsucc fzero) := tt              -- A → 1 → A 
|(fsucc fzero) fzero (fsucc (fsucc fzero)):= tt       -- B → 0 → C
|(fsucc (fsucc fzero)) _ (fsucc (fsucc fzero)):= tt   -- C → 0 / 1 → C
|_ _ _ := ff

def nfa_1 : NFA Sigma1 := 
  {Q := Q1 , 
   init :=init1,
   final := final1,
   δ := δ1}

   
#reduce 1::2::[3]

def is_nfa_1 : Lang Sigma1
|nil := ff
|[fzero] := ff
|[fsucc fzero] := ff
|(fzero :: _ :: _) := ff
|(fsucc fzero :: fzero :: _):=tt
| _ := ff


def word_1: Word Sigma1 := [fzero, fzero.fsucc, fzero]
-- 010
def word_2: Word Sigma1 := [fzero, fzero, fzero]
-- 000                
def word_3: Word Sigma1 := [fzero, fzero]
-- 00
def word_4: Word Sigma1 := [fzero]
-- 0
def word_5: Word Sigma1 := [fzero.fsucc, fzero]
-- 10
def word_6: Word Sigma1 := [fzero.fsucc, fzero, fzero.fsucc]
-- 101
def word_7: Word Sigma1 := [fzero.fsucc]
-- 1
def word_8: Word Sigma1 := [fzero, fzero, fzero.fsucc, fzero.fsucc]
-- 0011

#reduce is_nfa_1 word_1
#reduce is_nfa_1 word_2
#reduce is_nfa_1 word_3
#reduce is_nfa_1 word_4
#reduce is_nfa_1 word_5
#reduce is_nfa_1 word_6
#reduce is_nfa_1 word_7
#reduce is_nfa_1 word_8
end nfa_beginwith10

namespace nfa_endwith10


def Sigma1 : FinSet := 2 -- {0, 1}
def Q1 : FinSet := 3 -- {A, B}
def A : El Q1 := fzero
def B : El Q1 := fzero.fsucc

def final1 : El Q1 → bool
|fzero := ff
|(fsucc fzero) := ff
|(fsucc (fsucc fzero)) := tt

def init1 : El Q1 → bool
|fzero:= tt
|(fsucc fzero) := ff
|(fsucc (fsucc fzero)) := ff

def δ1: El Q1 → El Sigma1 → El Q1 → bool
|fzero _ fzero:= tt
|fzero (fsucc fzero) (fsucc fzero) := tt                                                                    -- A → 0 → A 
|(fsucc fzero) fzero (fsucc (fsucc fzero)):= tt
|_ _ _ := ff

def nfa_1 : NFA Sigma1 := 
  {Q := Q1 , 
   init :=init1,
   final := final1,
   δ := δ1}

def is_nfa_1 : Lang Sigma1
|nil := ff
|[fsucc fzero, fzero] := tt
|(a::l):= is_nfa_1 l
-- (begin with 1,0)


def word_1: Word Sigma1 := [fzero, fzero.fsucc, fzero]
-- 010
def word_2: Word Sigma1 := [fzero, fzero, fzero]
-- 000
def word_3: Word Sigma1 := [fzero, fzero]
-- 00
def word_4: Word Sigma1 := [fzero]
-- 0
def word_5: Word Sigma1 := [fzero.fsucc, fzero]
-- 10
def word_6: Word Sigma1 := [fzero.fsucc, fzero, fzero.fsucc]
-- 101
def word_7: Word Sigma1 := [fzero.fsucc]
-- 1
def word_8: Word Sigma1 := [fzero, fzero, fzero.fsucc, fzero.fsucc]
-- 0011

#reduce is_nfa_1 word_1
#reduce is_nfa_1 word_2
#reduce is_nfa_1 word_3
#reduce is_nfa_1 word_4
#reduce is_nfa_1 word_5
#reduce is_nfa_1 word_6
#reduce is_nfa_1 word_7
#reduce is_nfa_1 word_8
end nfa_endwith10

namespace nfa_contain10


def Sigma1 : FinSet := 2 -- {0, 1}
def Q1 : FinSet := 3 -- {A, B}
def A : El Q1 := fzero
def B : El Q1 := fzero.fsucc

def final1 : El Q1 → bool
|fzero := ff
|(fsucc fzero) := ff
|(fsucc (fsucc fzero)) := tt

def init1 : El Q1 → bool
|fzero:= tt
|(fsucc fzero) := ff
|(fsucc (fsucc fzero)) := ff

def δ1: El Q1 → El Sigma1 → El Q1 → bool
|fzero _ fzero:= tt
|fzero (fsucc fzero) (fsucc fzero) := tt                                                                    -- A → 0 → A 
|(fsucc fzero) fzero (fsucc (fsucc fzero)):= tt
|(fsucc (fsucc fzero)) _ (fsucc (fsucc fzero)):= tt
|_ _ _ := ff

def nfa_1 : NFA Sigma1 := 
  {Q := Q1 , 
   init := init1,
   final := final1,
   δ := δ1}

def is_nfa_1 : Lang Sigma1
|nil := ff
|((fsucc fzero)::fzero::_) := tt
|(_::(fsucc fzero)::fzero::_) := tt
|_ := ff
-- (begin with 1,0)


def word_1: Word Sigma1 := [fzero, fzero.fsucc, fzero]
-- 010
def word_2: Word Sigma1 := [fzero, fzero, fzero]
-- 000
def word_3: Word Sigma1 := [fzero, fzero]
-- 00
def word_4: Word Sigma1 := [fzero]
-- 0
def word_5: Word Sigma1 := [fzero.fsucc, fzero]
-- 10
def word_6: Word Sigma1 := [fzero.fsucc, fzero, fzero.fsucc]
-- 101
def word_7: Word Sigma1 := [fzero.fsucc]
-- 1
def word_8: Word Sigma1 := [fzero, fzero, fzero.fsucc, fzero.fsucc]
-- 0011

#reduce is_nfa_1 word_1
#reduce is_nfa_1 word_2
#reduce is_nfa_1 word_3
#reduce is_nfa_1 word_4
#reduce is_nfa_1 word_5
#reduce is_nfa_1 word_6
#reduce is_nfa_1 word_7
#reduce is_nfa_1 word_8
end nfa_contain10

-- transform DFA to NFA
namespace test_DFA2NFA

def Sigma1 : FinSet 
  := 2
def a : El Sigma1 
  := fzero
def b : El Sigma1
  := fsucc fzero

def Q1 : FinSet := 2
-- 2 = {fzero, fsucc fzero}
def q0 : El Q1 := fzero
def q1 : El Q1 := fsucc fzero

def final1 : El Q1 → bool
| fzero := false 
| (fsucc fzero) := true
-- #check final1

def word_1: Word Sigma1 := [fzero, fzero.fsucc, fzero]
-- aba
def word_2: Word Sigma1 := [fzero, fzero, fzero]
-- aaa
def word_3: Word Sigma1 := [fzero, fzero]
-- aa
def word_4: Word Sigma1 := [fzero]
-- a
def word_5: Word Sigma1 := [fzero.fsucc, fzero]
-- ba
def word_6: Word Sigma1 := [fzero.fsucc, fzero, fzero.fsucc]
-- bab
def word_7: Word Sigma1 := [fzero.fsucc]
-- b


#reduce 3
-- def Lang_1: Lang Sigma1 := ∀ x : Word Sigma1, last character is 2


-- language of this 
-- def Lang_1 : Prop := (Lang Sigma1) word_1
#check Lang Sigma1

def δ1 : El Q1 → El Sigma1 → El Q1
--| q0 a := q1
| fzero fzero := q1
--| q0 b := q0
| fzero (fsucc fzero) := q0
--| q1 a := q1  
| (fsucc fzero) fzero := q1
--| q1 b := q0
| (fsucc fzero) (fsucc fzero) := q0
-- L = {a, ba, aab, aba, aaba, abba, .....}


end test_DFA2NFA


-- 1 means the element is true
-- 0 means the element is false

namespace dfa_4

-- structure DFA (Sigma : FinSet) :=
--   (Q : FinSet)
--   (init : El Q)
--   (final : El Q → bool)
--   (δ : El Q → El Sigma → El Q)
def Sigma3 : FinSet := 2
def Q3 : FinSet := 5
def final3 : El Q3 → bool
| fzero := tt
| (fsucc fzero) := tt
| (fsucc (fsucc fzero)) := tt
| (fsucc(fsucc (fsucc fzero))) := tt
| (fsucc (fsucc(fsucc (fsucc fzero)))) := ff

def δ3 : El Q3 → El Sigma3 → El Q3
| fzero fzero := fsucc fzero
| fzero (fsucc fzero) := fzero
| (fsucc fzero) fzero := (fsucc (fsucc fzero)) 
| (fsucc fzero) (fsucc fzero) := fzero
| (fsucc (fsucc fzero))  fzero := (fsucc (fsucc fzero)) 
| (fsucc (fsucc fzero))  (fsucc fzero) := fsucc (fsucc (fsucc fzero)) 
| (fsucc (fsucc (fsucc fzero))) fzero :=  (fsucc fzero) 
| (fsucc (fsucc (fsucc fzero))) (fsucc fzero) :=  (fsucc (fsucc (fsucc (fsucc fzero)))) 
| (fsucc (fsucc (fsucc (fsucc fzero)))) fzero :=  (fsucc (fsucc (fsucc (fsucc fzero)))) 
| (fsucc (fsucc (fsucc (fsucc fzero)))) (fsucc fzero) :=  (fsucc (fsucc (fsucc (fsucc fzero)))) 


def dfa_c : DFA Sigma3 := 
  {Q := Q3 , 
   init := fzero,
   final := final3,
   δ := δ3}


-- cannot enumerate it, too complicated
-- def is_dfa_c : Lang Sigma3
-- | nil := false


def word_1: Word Sigma3 := [fzero, fzero.fsucc, fzero]
-- aba
def word_2: Word Sigma3 := [fzero, fzero, fzero]
-- aaa
def word_3: Word Sigma3 := [fzero, fzero]
-- aa
def word_4: Word Sigma3 := [fzero]
-- a
def word_5: Word Sigma3 := [fzero.fsucc, fzero]
-- ba
def word_6: Word Sigma3 := [fzero.fsucc, fzero, fzero.fsucc]
-- bab
def word_7: Word Sigma3 := [fzero.fsucc]
-- b
def word_8: Word Sigma3 := [fzero, fzero, fzero.fsucc, fzero.fsucc]
-- aabb

def eval_dfa_c : Word Sigma3 → El Q3  → Prop 
| nil q := dfa_c.final q
| (a::l) q := eval_dfa_c l (dfa_c.δ q a)


#reduce eval_dfa_c word_1 dfa_c.init-- true -- aba
#reduce eval_dfa_c word_2 dfa_c.init-- true -- aaa
#reduce eval_dfa_c word_3 dfa_c.init-- true -- aa
#reduce eval_dfa_c word_4 dfa_c.init-- true-- a
#reduce eval_dfa_c word_5 dfa_c.init-- true -- ba
#reduce eval_dfa_c word_6 dfa_c.init-- true -- bab
#reduce eval_dfa_c word_7 dfa_c.init-- true -- b
#reduce eval_dfa_c word_8 dfa_c.init-- false -- aabb


-- structure NFA (Sigma : FinSet) :=
--   (Q : FinSet)  
--   (init : El Q → bool)
--   (final : El Q → bool)
--   (δ : El Q → El Sigma → El Q → bool)
def  nfa_test := dfa2nfa dfa_c

#reduce nfa_test
#reduce NFA_Lang nfa_test word_1-- true -- aba
#reduce NFA_Lang nfa_test word_2-- true -- aaa
#reduce NFA_Lang nfa_test word_3-- true -- aa
#reduce NFA_Lang nfa_test word_4-- true-- a
#reduce NFA_Lang nfa_test word_5-- true -- ba
#reduce NFA_Lang nfa_test word_6-- true -- bab
#reduce NFA_Lang nfa_test word_7-- true -- b
#reduce NFA_Lang nfa_test word_8-- false -- aabb

end dfa_4

-- NFA to DFA 
-- subset of NFA.Q is DFA.Q
-- states in NFA
-- start with the initial state. (if more than one initial state, combine them)
def natpow : ℕ → ℕ → ℕ
| a 0 := 1
| a (succ b) := a * (natpow a b)

-- def FinPow: -- sequence of bits
-- NFA.Q = {q0, q1 ,q2, q3}
-- DFA.Q = { {q0}, {q0, q1}, {q2,q3}}
-- DFA.Q = { 0001, 0011, 1100 }

-- fin2nat NFA.Q (El NFA.Q) 





