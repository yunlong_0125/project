import finite_set
import data.fin.basic
import data.list.basic
import data.multiset
import data.finset


def S : set ℕ := {n : ℕ | 4 ≤ n}
def m : finset ℕ := {1,2,3}
def X : finset bool := { ff, tt, ff}

def y : ℕ := 5
#check y
#reduce y

#check {1, 2, 3, 4}
#check m
#check multiset
#reduce multiset S

#reduce finset S
#reduce finset bool
#check set
#check list
#check finset ℕ 
#check finset S
#check Type*
#check Type

-- structure point (α : Type*) :=
-- mk :: (x : α) (y : α)

-- -- BEGIN
-- #reduce point.x (point.mk 10 20)
-- #reduce point.y (point.mk 10 20)

-- open point

-- example (α : Type*) (a b : α) : x (mk a b) = a :=
-- begin
  

-- end
-- example (α : Type*) (a b : α) : y (mk a b) = b :=
-- rfl
-- -- END


-- -- BEGIN
-- #check point              -- a Type
-- #check point.rec_on       -- the eliminator
-- #check point.x            -- a projection / field accessor
-- #check point.y            -- a projection / field accessor
-- -- END
universe u

structure point (α : Type u) :=
mk :: (x : α) (y : α)

structure {v} point2 (α : Type v) :=
mk :: (x : α) (y : α)

structure point3 (α : Type _) :=
mk :: (x : α) (y : α)


#check @point ℕ
#check @point2
#check @point3
#check "q0"



namespace DFA_working

variables P: Prop
  -- structure DFA :=
  -- mk :: (Q : finset char) (Sigma : finset char)
  -- (initial : char) (final : char → bool) (δ : char → Q → char)

  structure DFA (state : char):=
  mk :: (Q : finset state) (Sigma : finset char)
  (initial : state) (final : state → bool) (δ : char → state → char)


-- initial ∈ Sigma
structure DFA  (Q : finset char):=
mk ::  (Sigma : finset char)
(initial : char) (final : char → bool) (δ : Q → char → Q)

structure DFA ( Sigma : finset ) :=
  Q :: finset
  initial :: Q
  final :: Q → Prop
  δ :: Q → Sigma → Q
-- final should not be Q here, but one of the members in Q.
-- Q a finite set of states
-- Σ a finite set of input symbols, the alphabet, Σ
-- δ A transition function δ ∈ Q × Σ → Q
-- initial An initial state ∈ Q
-- final A set of final states F ⊆ Q
-- how to represent one of the element in the finset
-- #check 
def char_var : char := 'a'
def final : char → Prop := char_var → true
#check DFA.mk {'A', 'B', 'C'} {'a', 'b', 'c'} 'A' ('C'→P)
-- #check DFA.mk {'a','b','c'} 
  -- Q :: finset char
  -- initial :: Q
  -- final :: Q → Prop
  -- δ :: Q → Sigma → Q


def Sigma : finset char := {'0', '1', '2'}
def States : finset char := {'A', 'B', 'C'}

def a : char := 'a'
-- def initial : States := 


def Q : finset char := {'A', 'B', 'C'}

#check DFA {'A', 'B', 'C'}

#check DFA
#check DFA
def New_DFA : DFA Sigma := DFA.mk States  

  -- Q :: finset char
  -- alphabat :: finset char
  -- initial :: Q
  -- final :: Q → Prop
  -- δ :: Q → Sigma → Q












def Word (Sigma : finset ) := List Sigma
-- can be defined as a list of elements from sigma

def δext : Q → Word → Prop
-- to be defined




def Lang (Sigma : finset)
  := List Sigma → Prop

  
DFA_Lang : DFA Sigma → Lang Sigma
-- to be defined
-- 





evalDFA :: DFA st -> String -> Bool
-- initial state, run δext, final state

-- example automata
  
end DFA_working



namespace practice

variables P Q R : Prop
variables A B C : Type
variables PP QQ : A → Prop

-- BEGIN
example : (∀ x : A, PP x ∧ QQ x)
  ↔ (∀ x : A , PP x) ∧ (∀ x : A, QQ x) :=
  begin
    constructor,
    assume h,
    constructor,
    assume a,
    have m : PP a ∧ QQ a,
    apply h,
    cases m with p q,
    apply p,
    assume a,
    have m : PP a ∧ QQ a,
    apply h,
    cases m with p q,
    apply q,
    assume h,
    cases h with h1 h2,
    assume a,
    constructor,
    apply h1,
    apply h2,
  end




-- END


structure point (α : Type*) :=
mk :: (x : α) (y : α)
#check point.mk 10 20
-- BEGIN
#reduce point.x (point.mk 10 20)
#reduce point.y (point.mk 10 20)

open point

example (α : Type*) (a b : α) : x (mk a b) = a :=
rfl

example (α : Type*) (a b : α) : y (mk a b) = b :=
rfl
-- END


end practice