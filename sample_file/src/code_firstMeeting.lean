structure DFA ( Sigma : finset ) :=
  Q :: finset
  initial :: Q
  final :: Q → Prop
  δ :: Q → Sigma → Q

def Word (Sigma : finset) := List Sigma

def Lang (Sigma : finset)
  := List Sigma → Prop

def δext : Q → Word → Prop
-- to be defined
  
DFA_Lang : DFA Sigma → Lang Sigma
-- to be defined

-- initial state, run δext, final state

-- example automata
  
