/-
COMP2009-ACE

Exercise 05 (Natural numbers)

    This exercise has 2 parts both count for 50%.

    In the first part the goal is to complete the proof that the
    natural numbers with addition and multiplication form a
    semiring. I include the proof the addition forms a commutative
    monoid (which we have done in the lecture.

    You are not supposed to use the ring tactic for the first part
    (otherwise it would be no challenge).

    Yes, you may need some additional lemmas.

    In the 2nd part you should show that ≤ is anti-symmetric. I
    include the proofs (from the lecture) that it is reflexive and
    transitive. You are allowed to use the ring tactic for this part. 
    (but note that you have to create a lean project to access the
    tactic library).

    You create a lean project using 
    leanproject new my_project
    which creates a folder my_project. You need to stire the exercise
    in my_project/src
    See https://leanprover-community.github.io/install/project.html
    for details.

    However, if you work with the web interface you don't need to
    create a project - ity should work out of the box.

    Please only submit the lean file not the whole project directory.

-/
import tactic -- will fail if you haven't created a project.
set_option pp.structure_projections false

namespace ex05_1

open nat

-- definition of addition:
def add : ℕ → ℕ → ℕ 
| n zero     := n
| n (succ m) := succ (add n m)



local notation m + n := add m n

-- have shown that it is a commutative monoid

theorem add_rneutr : ∀ n : ℕ, n + 0 = n :=
begin
  assume n,
  refl,
end

theorem add_lneutr : ∀ n : ℕ, 0 + n  = n :=
begin
  assume n,
  induction n with n' ih,
  refl,
  dsimp [(+),add],
  rewrite ih,
end

theorem add_assoc : ∀ l m n : ℕ , (l + m) + n = l + (m + n) :=
begin
  assume l m n,
  induction n with n' ih,
  refl,
  dsimp [(+),add],
  rewrite ih,
end

lemma add_succ_lem : ∀ m n : ℕ, succ m + n = succ (m + n) :=
begin
  assume m n,
  induction n with n' ih,
  refl,
  apply congr_arg succ,
  exact ih,
end

theorem add_comm : ∀ m n : ℕ , m + n = n + m :=
begin
  assume m n,
  induction m with m' ih,
  apply add_lneutr,
  calc 
    succ m' + n = succ (m' + n) : by apply add_succ_lem
    ... = succ (n + m') : by apply congr_arg succ; exact ih
    ... = n + succ m' : by reflexivity,
end

-- now we define addition

def mul : ℕ → ℕ → ℕ
| m 0     := 0
| m (succ n) := (mul m n) + m


local notation m * n := mul m n

-- and your task is to show that it is a commutative semiring, i.e.

theorem mult_rneutr : ∀ n : ℕ, n * 1 = n :=
begin
  dsimp[(*),mul],
  apply add_lneutr,
end

theorem mult_lneutr : ∀ n : ℕ, 1 * n  = n :=
begin
  assume n,
  induction n with n' ih,
  refl,

  dsimp[(*),mul],
  rewrite ih,
  dsimp[(*),mul,(+)],
  refl,

end

theorem mult_zero_l : ∀ n : ℕ , 0 * n = 0 :=
begin
  assume n,
  induction n with n' ih,
  refl,
  dsimp [(*),add],
  exact ih,
end 

theorem mult_zero_r : ∀ n : ℕ , n * 0 = 0 :=
begin
  assume n,
  refl,
end


theorem mult_distr_r :  ∀ l m n : ℕ , l * (m + n) = l * m + l * n :=
begin
  assume l m n,
  induction n with n' ih,
  dsimp[add,mul],
  refl,

  dsimp[add,mul],
  rewrite ih,
  apply add_assoc,
  
end


theorem mult_distr_l :  ∀ l m n : ℕ , (m + n) * l = m * l + n * l :=
begin
  assume l m n,
  induction l with l' ih,
  refl,

  dsimp[mul],
  rewrite ih,
  -- m * l' + n * l' + (m + n) = m * l' + m + (n * l' + n)
  have k: n * l' + n= n+ n*l',
  rw add_comm,
  have h: n * l' + (m + n) =  m + (n * l' + n),

  calc 
    n * l' + (m + n) = m + n + n * l' : by apply add_comm
    ... = m + ( n + n * l') : by apply add_assoc
    ... = m + (n * l' + n) : by rewrite k,

  calc
    m * l' + n * l' + (m + n) = m * l' + ( n * l' + (m + n)):by apply add_assoc
    ...= m * l' + ( m + (n * l' + n) ): by rewrite h
    ...= m * l' + m + (n * l' + n) : by symmetry; apply add_assoc,
end


theorem mult_assoc : ∀ l m n : ℕ , (l * m) * n = l * (m * n) :=
begin
  assume l m n,
  induction n with n' ih,
  refl,

  dsimp[mul],
  symmetry,
  rewrite ih,
  apply mult_distr_r,
end

theorem mult_comm :  ∀ m n : ℕ , m * n = n * m :=
begin
  assume m n,
  induction m with m' ih,

  apply mult_zero_l,
  dsimp[mul],
  rewrite←  ih,
  have j : 1*n = n,
  apply mult_lneutr,
  have k :  m' * n + n = (m'+1) * n,
    calc  m' * n + n =  m' * n + 1*n : by rewrite  j
    ...= (m'+1) * n : by rewrite← mult_distr_l,

  rewrite k,
  refl,
end
end ex05_1


namespace ex05_2
-- part 2
-- we define ≤ as follows


def le(m n : ℕ) : Prop :=
  ∃ k : ℕ , k + m = n


local notation x ≤ y := le x y
local notation x ≥ y := le y x

-- and we have shown that it is a preorder, i.e. reflexive and transitive:
-- note that we have used the ring tactic to do all the equational reasoning:

theorem le_refl : ∀ x : ℕ , x ≤ x :=
begin
  assume x,
  existsi 0,
  ring,
end


theorem le_trans : ∀ x y z : ℕ , x ≤ y → y ≤ z → x ≤ z :=
begin
  assume x y z xy yz,
  cases xy with k p,
  cases yz with l q,
  existsi (k+l),
  rewrite← q,
  rewrite← p,
  ring,
end

-- Your task is to show that ≤ is antisymmetric, and hence a *partial order*
-- you are allowed to use the ring tactic.
-- Yes, you may need some lemmas!



lemma helper1 : ∀ x y : ℕ  , x + y = 0 → x = 0 ∧ y = 0 :=
begin
    assume x y,
    assume h,
    constructor,
    induction y with y' ih,
    apply h,

    contradiction,

    cases y,
    refl,
    contradiction,


end


lemma helper2 : ∀ x y : ℕ ,y + x = x → y = 0 :=
begin

    assume x y,
    assume h,
    induction x with x' ih,
    apply h,
    apply ih,
    injection h,

end


theorem anti_sym : ∀ x y : ℕ , x ≤ y → y ≤ x → x = y :=
begin
  assume x y,
  dsimp[(≤)],
  assume xley,
  assume ylex,

  cases xley with h1 h2,
  cases ylex with g1 g2,
  
  rewrite← h2 at g2,

  rewrite←  add_assoc at g2,

  have t: g1 + h1 = 0,
  apply helper2,
  apply g2,

  have k : g1 = 0 ∧ h1 = 0 ,
  apply helper1,
  exact t,
  
  cases k with k1 k2,
  rewrite k2 at h2,

  have j: 0 + x = x,
  ring,

  rewrite j at h2,
  exact h2, 
  

end


end ex05_2
